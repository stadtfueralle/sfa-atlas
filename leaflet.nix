[
  ({ stdenv, fetchzip }: stdenv.mkDerivation rec {
    pname = "leaflet";
    version = "1.9.4";
    src = fetchzip {
      url = "http://cdn.leafletjs.com/${pname}/v${version}/leaflet.zip";
      sha256 = "sha256-vThq8wIpm1wNrNgR3Epd82nwlc1XCtqztrXhEJILVC4=";
      stripRoot = false;
    };
    installPhase = ''
          mkdir -p $out
          cp -vr $src $out/${pname}
        '';
  })
  ({ stdenv, fetchzip }: stdenv.mkDerivation rec {
    pname = "Leaflet.markercluster";
    version = "1.5.3";
    src = fetchzip {
      url = "https://github.com/Leaflet/${pname}/archive/v${version}.zip";
      sha256 = "sha256-P4FEvG4zwDje9jdx4a5xq7z9m8fwYV1zqUnqZ4LYG7s=";
      stripRoot = false;
    };
    installPhase = ''
          mkdir -p $out
          cp -vr $src/${pname}-${version}/dist $out/${pname}
        '';
  })
  ({ stdenv, fetchzip }: stdenv.mkDerivation rec {
    pname = "Leaflet.Deflate";
    version = "2.1.0";
    src = fetchzip {
      url = "https://github.com/oliverroick/${pname}/archive/refs/tags/v${version}.zip";
      sha256 = "sha256-YfmG0QDSniSsgSr1dcnQFh5A5FDGqq6ZidDRMG/fpf4=";
      stripRoot = false;
    };
    installPhase = ''
          mkdir -p $out
          cp -vr $src/${pname}-${version}/dist $out/${pname}
        '';
  })
]
