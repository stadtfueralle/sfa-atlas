{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE InstanceSigs      #-}

module ApiGeoFeature (
                     API, server
                     )where

import           Control.Monad.IO.Class          (liftIO)
import           Data.Aeson                      (Value, ToJSON, object, toJSON, (.=))
import           Data.Geometry.Geos.Geometry     (Some (Some))
import           Data.Maybe                      (fromMaybe)
import qualified Data.Text                       as T
import           Data.Map.Append                 (AppendMap (..))
import           Data.Map.Strict                 (singleton, elems)
import           Database.Esqueleto.Experimental (Entity (..), entityKey, entityVal)
import           GHC.TypeLits                    (Symbol)
import           GHC.Generics                    (Generic)
import           Servant

import qualified Database               as Db
import qualified DatabaseGebaeude       as DbG
import qualified DatabaseParzelle       as DbP
import           PostgisHaskellBinding  (Geo)
import qualified SchemaEingang          as SchE
import qualified SchemaGebaeude         as SchG
import qualified SchemaParzelle         as SchP

-- a data type that will encoded to GeoJSON feature
data GeoFeature = GeoFeature { geometry :: Some Geo
                             , properties :: Fprops
                             }
instance ToJSON GeoFeature where
  toJSON :: GeoFeature -> Value
  toJSON (GeoFeature geo prop) = object
    [ "geometry"    .= geo
    , "type"        .= ("Feature" :: String)
    , "properties"  .= toJSON prop
    ]

data FeaturePropsGeba = FeaturePropsGeba { egid   :: Double
                                         , gkat :: Maybe T.Text
                                         } deriving (Show, Generic, ToJSON)

data FeaturePropsEing = FeaturePropsEing { edid    :: Int
                                         , gebEgid :: Double
                                         , address :: T.Text
                                         } deriving (Show, Generic, ToJSON)

data FeaturePropsParz = FeaturePropsParz { egrid     :: T.Text
                                         , typ       :: T.Text
                                         , kategorie :: Maybe T.Text
                                         } deriving (Show, Generic, ToJSON)

data Fprops = FpropsP FeaturePropsParz
            | FpropsE FeaturePropsEing 
            | FpropsG FeaturePropsGeba 

instance ToJSON Fprops where
  toJSON :: Fprops -> Value
  toJSON props = case props of
    FpropsP p -> toJSON p
    FpropsE p -> toJSON p
    FpropsG p -> toJSON p


newtype GebaeudeInter = GebaeudeInter ( Entity SchG.Gebaeude
                                      , Maybe (Entity SchG.KategorieInfo)
                                      )

newtype ParzelleInter = ParzelleInter ( Entity SchP.Parzelle
                                      , Maybe (Entity SchP.Kategorie)
                                      )

instance Semigroup GebaeudeInter where
  GebaeudeInter (p1, k1) <> GebaeudeInter (p2, k2) = GebaeudeInter (p1, k1)

instance Semigroup ParzelleInter where
  ParzelleInter (p1, k1) <> ParzelleInter (p2, k2) = ParzelleInter (p1, k1)

type GebaeudeMap = AppendMap SchG.GebaeudeId GebaeudeInter

type ParzelleMap = AppendMap SchP.ParzelleId ParzelleInter

eingToFeature :: SchE.Eingang -> GeoFeature
eingToFeature (SchE.Eingang geom _ gebaeudeEgid edid strasse nummer _) =
  GeoFeature (Some geom) (FpropsE props)
     where props = FeaturePropsEing edid gebaeudeEgid (strasse <>" "<> nummer)

gebaSqlRowsToFeatures :: [DbG.GebaeudeSql] -> [GeoFeature]
gebaSqlRowsToFeatures gSqlL =
  let map = mconcat $ gebaInterToSingletonMap . gebaSqlToInter <$> gSqlL
  in interToGebaeude <$> mapToGebaeude map

gebaSqlToInter :: DbG.GebaeudeSql -> GebaeudeInter
gebaSqlToInter (g, mbI, mbK) = GebaeudeInter (g, mbK)

gebaInterToSingletonMap :: GebaeudeInter -> GebaeudeMap
gebaInterToSingletonMap gInter@(GebaeudeInter(g, mbK)) =
  AppendMap $ singleton (entityKey g) gInter

mapToGebaeude :: GebaeudeMap -> [GebaeudeInter]
mapToGebaeude = elems . unAppendMap

interToGebaeude :: GebaeudeInter -> GeoFeature
interToGebaeude (GebaeudeInter (Entity pKey g, mbK)) =
  GeoFeature (Some $ SchG.gebaeudeSimpleGeom g) (FpropsG props)
     where props = FeaturePropsGeba  
             (SchG.gebaeudeEgid g)
             (SchG.kategorieInfoKategorie . entityVal <$> mbK)

sqlRowsToParzList :: [DbP.ParzelleSql] -> [GeoFeature]
sqlRowsToParzList pSqlL =
  let map = mconcat $ interToSingletonMap . sqlToInter <$> pSqlL
  in interToParzelle <$> mapToParzelle map

sqlToInter :: DbP.ParzelleSql -> ParzelleInter
sqlToInter (p, mbE, mbB, mbK) = ParzelleInter (p, mbK)

interToSingletonMap :: ParzelleInter -> ParzelleMap
interToSingletonMap pInter@(ParzelleInter(p, mbK)) =
  AppendMap $ singleton (entityKey p) pInter

mapToParzelle :: ParzelleMap -> [ParzelleInter]
mapToParzelle = elems . unAppendMap

interToParzelle :: ParzelleInter -> GeoFeature
interToParzelle (ParzelleInter (Entity pKey p, mbK)) =
  GeoFeature (Some $ SchP.parzelleSimpleGeom p) (FpropsP props)
     where props = FeaturePropsParz  
             (SchP.parzelleEgrid p)
             (SchP.parzelleTyp p)
             (SchP.kategorieName . entityVal <$> mbK)

-- | List of features aka GeoJSON feature collection served by api
newtype FeatureCollection = FeatureCollection { features :: [GeoFeature] }

instance ToJSON FeatureCollection where
  toJSON fc = object
    [ "type" .= ("FeatureCollection" :: String)
    , "features"  .= (toJSON <$> features fc)
    ]

-- | an endpoint named after a geo object. takes a region as query parameter and
-- | returns a json value (in fact a GeoJson FeatureCollection)
type GenericAPI (name :: Symbol) = name :>
  QueryParam "region" String :> Get '[JSON] FeatureCollection

type API = GenericAPI "liegenschaft"
      :<|> GenericAPI "baurecht"
      :<|> GenericAPI "gebaeude"
      :<|> GenericAPI "eingang"

parzellenHandler :: T.Text -> Maybe String -> Handler FeatureCollection
parzellenHandler typ mbRegion = do
  qParz <- liftIO $ DbP.qParzList typ (T.pack $ fromMaybe "Basel" mbRegion)
  liftIO $ print "finishe parzelle query"
  pure . FeatureCollection $ sqlRowsToParzList qParz

gebaeudeHandler :: Maybe String -> Handler FeatureCollection
gebaeudeHandler mbRegion = do
  qGeb <- liftIO $ DbG.qGebaList (T.pack $ fromMaybe "Basel" mbRegion)
  liftIO $ print "finishe Gebaeude query"
  pure . FeatureCollection $ gebaSqlRowsToFeatures qGeb

eingangHandler :: Maybe String -> Handler FeatureCollection
eingangHandler mbRegion = do
  qEingang <- liftIO $ Db.peelValues $
    Db.qGeoInRegion "eingang" (T.pack $ fromMaybe "Basel" mbRegion)
  pure . FeatureCollection $ eingToFeature <$> qEingang

server :: Server API
server = parzellenHandler "Liegenschaft"
    :<|> parzellenHandler "Baurecht"
    :<|> gebaeudeHandler
    :<|> eingangHandler
