{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module SchemaUser where

import           Data.Aeson
import           Data.Aeson.Types
import           Data.Text
import qualified Database.Persist.Sql as Sql
import qualified Database.Persist.TH  as PTH
import           Servant.Auth.Server  (FromJWT, ToJWT)

import           Database             (runActionR)

PTH.share [PTH.mkPersist PTH.sqlSettings, PTH.mkEntityDefList "entityDefs"] [PTH.persistLowerCase|
  User sql=users
    name Text
    email Text
    UniqueEmail email
    deriving Show Read
|]

instance ToJSON User where
  toJSON user = object
    [ "name" .= userName user
    , "email" .= userEmail user
    ]

instance FromJSON User where
  parseJSON = withObject "User" parseUser

instance ToJWT User
instance FromJWT User

parseUser :: Object -> Parser User
parseUser o = do
  uName <- o .: "name"
  uEmail <- o .: "email"
  return User
    { userName = uName
    , userEmail = uEmail
    }

fetchUserByEmail :: Text -> IO (Maybe (Sql.Entity User))
fetchUserByEmail mail = do
  runActionR (Sql.getBy $ UniqueEmail mail)
