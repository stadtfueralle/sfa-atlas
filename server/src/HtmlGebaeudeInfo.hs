{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlGebaeudeInfo ( infoForm
                        , infoDisplay
                        , infoListDisplay
                        ) where

import           Data.Double.Conversion.Text (toFixed)
import           Data.Maybe                  (isJust)
import qualified Data.Text                   as T
import           Data.Time                   (showGregorian, utctDay)
import           Lucid

import           HtmlGeneric                 (deleteButton, editButton,
                                              inputDate, inputNumber,
                                              itemDisplay, lTxtInput,
                                              listDisplay, selectMultiField,
                                              sTxtInput, saveButton,
                                              withdrawButton)
import           SchemaUser                  (User)
import           ViewGebaeudeModel           (InfoShow (..))
import           ViewGeneric                 (SfaData, toDisplay)

infoForm :: InfoShow -> Html ()
infoForm info =
  let object = "info"
      (id', datum, egid, absender, bemerk, katL) = serializeInfo info
  in form_ [ data_ "object" object, data_ "id" id' ] $ do
    figure_ [ class_ "card" ] $ do
      figcaption_ [ ] $ do
        div_ [ class_ "register" ] $ do
          div_ [ data_ "type" "date" ] $ do
            inputDate "datum" "inDatum" True (Just datum)
          div_ [ class_ "edit-buttons" ] $ do
            saveButton
            withdrawButton
      ul_ [] $ do
        input_ [  name_ "inId"
                , required_ "true"
                , value_ id'
                , type_ "hidden"
                ]
        li_ [ data_ "type" "str"] $ do
          legend_ [ for_ "gebaeude-egid" ] "Gebaeude Egid"
          inputNumber "gebaeude-egid" "inGebEgid" True (Just egid)
        li_ [ data_ "type" "list" ] $ do
          fieldset_ [ form_ ("info-" <> id'), name_ "inKatIdList" ] $ do
            legend_ [] "Kategorie"
            selectMultiField  "inKatIdList" True katL
        li_ [ data_ "type" "str" ] $ do
          legend_ [ ] "Absender"
          sTxtInput False "absender" "inAbsender" absender

        li_ [ data_ "type" "text"] $ do
          legend_ [] "Bemerkung"
          lTxtInput True "bemerkung" "inText" (Just bemerk)


infoListDisplay :: SfaData InfoShow => Maybe User -> [InfoShow] -> Html ()
infoListDisplay mbUsr iL = listDisplay (toDisplay mbUsr <$> iL)

infoDisplay :: Maybe User -> InfoShow -> Html ()
infoDisplay mbUser i =
  let
    object           = "info"
    isEditable       = isJust mbUser
    editButtonsClass = if isEditable then "edit-buttons"
                                     else "edit-buttons blank"
    (id', datum, egid, absender, bemerk, katList) = serializeInfo i
  in figure_ [ class_ "card display"
             , data_ "id" id'
             , data_ "object" object 
             ] $ do
       figcaption_ [] $ do
         div_ [ data_ "type" "date", class_ "register" ] $ do
           span_ [ class_ "value" ] (toHtml datum)
           div_ [ class_ editButtonsClass ] $ do
             editButton
             deleteButton id' object
       ul_ [] $ do
         itemDisplay "str" (Just "Gebaeude Egid") False $ Just egid
         itemDisplay "str" (Just "Kategorie") False
                           (Just $ T.intercalate ", " (snd <$> katList))
         itemDisplay "str" (Just "Absender") False absender
         itemDisplay "text" (Just "Bemerkung") False $ Just bemerk


serializeInfo :: InfoShow -> ( T.Text, T.Text, T.Text, Maybe T.Text
                                 , T.Text, [(T.Text, T.Text)])
serializeInfo i = ( T.pack. show $ outId i
                  , T.pack . showGregorian . utctDay $ outDatum i
                  , toFixed 0 $ outGebEgid i
                  , outAbsender i
                  , outText i
                  , map (\(a,b) -> (T.pack $ show a, b)) $ outKatList i
                  )
