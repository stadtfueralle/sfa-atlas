module DbRooseli ( migrate
                 , qEingangByGebaeude
                 , qGebaeudeByEgid
                 ) where

import           Database.Persist.Class.PersistEntity (Entity (..))
import           Database.Persist.Sql                 (entityDef, getBy,
                                                       selectList, (==.))
import qualified Database.Persist.Sql.Migration       as Mig

import qualified Database                             as Db
import qualified SchemaEingang                        as SchE
import qualified SchemaGebaeude                       as SchG
import qualified SchemaParzelle                       as SchP
import qualified SchemaUser                           as BSch

migrate :: IO ()
migrate = do
  Db.runActionR $ Mig.runMigration $ Mig.migrate SchE.entityDefs $
    entityDef (Nothing :: Maybe SchE.Eingang)
  Db.runActionR $ Mig.runMigration SchG.migrateAll
  Db.runActionR $ Mig.runMigration SchP.migrateAll
  Db.runActionR $ Mig.runMigration $ Mig.migrate BSch.entityDefs $
    entityDef (Nothing :: Maybe BSch.User)

qGebaeudeByEgid :: Double -> IO (Maybe (Entity SchG.Gebaeude))
qGebaeudeByEgid egid = do
  Db.runActionR (getBy (SchG.UniqueEgid egid))

-- | Not sure if an Eingang is spatially always inside its Gebaeude polygon.
-- Maybe a portal could also be outside of a gebaeude polygon? To make sure, I
-- added the attribute gebaeudeEgid to the Eingang schema
qEingangByGebaeude :: SchG.Gebaeude -> IO [Entity SchE.Eingang]
qEingangByGebaeude g = do
  Db.runActionR $
    selectList [SchE.EingangGebaeudeEgid ==. SchG.gebaeudeEgid g] []
