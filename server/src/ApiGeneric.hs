{-# LANGUAGE AllowAmbiguousTypes  #-}
{-# LANGUAGE ConstraintKinds      #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeApplications     #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeSynonymInstances #-}


module ApiGeneric where

import           Control.Monad              ((>=>))
import           Control.Monad.IO.Class     (liftIO)
import           Control.Monad.Trans.Except (throwE)
import           Data.Int                   (Int64)
import           Servant                    (Capture, Delete, Get, Handler (..),
                                             JSON, NoContent (..), Patch, Post,
                                             ReqBody, Server, err401, errBody,
                                             (:<|>) (..), (:>))
import           Servant.Auth.Server        (Auth, AuthResult (Authenticated),
                                             throwAll)

import qualified Database                   as Db
import qualified Database.Persist.Sql       as Sql
import qualified Database.Persist.TH        as PTH
import           SchemaUser                 (User)

type User' = User
instance Db.SqlClass User'

delete :: forall b . Db.SqlClass b => Int64 -> Handler NoContent
delete id' =
  let kinfo = Sql.toSqlKey id' :: Sql.Key b
  in (liftIO . Db.runActionR $ Sql.delete kinfo) >> pure NoContent

list :: Db.SqlClass a => Handler [a]
list = fmap (\(Sql.Entity _ p) -> p) <$> liftIO Db.qRecordList
-- list = fmap snd <$> liftIO Db.qRecordList

update :: Db.SqlClass a => Int64 -> a -> Handler Int64
update recId record = liftIO $ Db.updateRecord recId record

create :: Db.SqlClass a => a -> Handler Int64
create record = liftIO $ Db.insertUnique record

byId :: Db.SqlClass a => Int64 -> Handler a
byId = fetchEntry >=> handleEntry
  where
    err = throwE $ err401 { errBody = "Could not find record with that ID" }
    fetchEntry (usrid :: Int64) = liftIO $ Db.qRecordById usrid
    handleEntry mbEntry =
      case mbEntry of
        Just entry -> return entry
        Nothing    -> Handler err

type Protected a = "create" :> ReqBody '[JSON] a
                            :> Post '[JSON] Int64
              :<|> "update" :> Capture "id" Int64
                            :> ReqBody '[JSON] a
                            :> Patch '[JSON] Int64
              :<|> "delete" :> Capture "id" Int64
                            :> Delete '[JSON] NoContent
              :<|> "list"   :> Get  '[JSON] [a]
              :<|> "get"    :> Capture "id" Int64
                            :> Get  '[JSON] a

type API a auths = Auth auths User' :> Protected a

protected :: forall a . Db.SqlClass a => AuthResult User' -> Server (Protected a)
protected (Authenticated _) = create :<|> update :<|> delete @a :<|> list :<|> byId
protected _                 = throwAll err401

server :: Db.SqlClass a => Server (API a auths)
server = protected
