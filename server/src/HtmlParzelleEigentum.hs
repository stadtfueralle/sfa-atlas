{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlParzelleEigentum ( form
                            , display
                            ) where

import           Data.Maybe        (fromMaybe, isJust)
import qualified Data.Text         as T
import           Data.Int          (Int64)
import           Data.List         (partition)
import           Data.Time         (showGregorian, utctDay)
import           Lucid

import           HtmlGeneric       (deleteButton, editButton, inputDate,
                                    itemDisplay, listDisplay, selectMultiField,
                                    selectSingleField, lTxtInput, sTxtInput, saveButton,
                                    withdrawButton)
import           SchemaUser        (User)
import           ViewGeneric       (SfaData, toDisplay)
import           ViewParzelleModel (EigentumShow (..), Kategorie (..))

display :: Maybe User -> EigentumShow -> Html ()
display mbUser e =
  let
    object           = "eigentum"
    isEditable       = isJust mbUser
    (id', datum, egrid, katList, bemerk, besitz, _, besName, besBemerk, besOrt)
                     = serializeEigen' e
    showConfidential = isJust mbUser ||
                       "Privatperson" `notElem` katList
  in figure_ [ class_ "card display"
             , data_ "id" id'
             , data_ "object" object 
             ] $ do
       figcaption_ [ class_ "head" ] $ do
         div_ [ data_ "type" "date", class_ "register" ] $ do
           span_ [ class_ "value" ] (toHtml datum)
           if isEditable
           then div_ [ class_ "edit-buttons" ] $ do
                  editButton
                  deleteButton id' object
           else mempty
       ul_ [] $ do
         itemDisplay "str" (Just "EGRID") False $ Just egrid
         itemDisplay "str" (Just "Quelle") False $ Just bemerk
         if showConfidential
         then itemDisplay "text"  (Just "Kommentar Besitz") False $ Just besitz
         else mempty
         figure_ [] $ do
           legend_ [] "Besitzanspruch"
           ul_ [ style_ "display: flex; flex-wrap: wrap" ] $ do
             if showConfidential
             then do
               itemDisplay "text" (Just "Name")             False besName
               itemDisplay "str"  (Just "Kategorie") False $
                                         Just $ T.intercalate ", " katList
               itemDisplay "str"  (Just "Ort")              False besOrt
               itemDisplay "text" (Just "Bemerkung")        False besBemerk
             else 
               itemDisplay "str"  (Just "Kategorie") False $
                                         Just $ T.intercalate ", " katList

form :: EigentumShow -> Html ()
form eigen =
  let 
    (id', datum, egrid, katList, bemerk, besitz, besId, besName, _, _)
             = serializeEigen' eigen
  in form_ [ data_ "object" "eigentum", data_ "id" id' ] $ do
       figure_ [ class_ "card" ] $ do
         figcaption_ [ ] $ do
           div_ [ class_ "register" ] $ do
             div_ [ data_ "type" "date" ] $ do
               inputDate "datum" "eInDatum" True (Just datum)
             div_ [ class_ "edit-buttons" ] $ do
               saveButton
               withdrawButton
         ul_ [] $ do
           input_ [  name_ "eInId"
                   , required_ "true"
                   , value_ id'
                   , type_ "hidden"
                   ]
           li_ [ data_ "type" "str"] $ do
             legend_ [] "Parzelle Egrid"
             sTxtInput True "parzelle-egrid" "eInParzEgrid" (Just egrid)
           li_ [ data_ "type" "text" ] $ do
             legend_ [] "Kommentar"
             lTxtInput True "besitz" "eInBesitz" (Just besitz)
           li_ [ data_ "type" "str" ] $ do
             legend_ [] "Quelle"
             sTxtInput True "bemerkung" "eInBemerk" (Just bemerk)
           li_ [ data_ "type" "text"] $ do
             fieldset_ [ name_ "eInBesId" ] $ do
               legend_ [] "Besitzende"
               selectSingleField "eInBesId"( fromMaybe "0" besId
                                          , fromMaybe "" besName
                                          )

serializeEigen' :: EigentumShow
               -> ( T.Text, T.Text, T.Text, [T.Text], T.Text, T.Text
                  , Maybe T.Text, Maybe T.Text , Maybe T.Text, Maybe T.Text )
serializeEigen' e = ( T.pack . show $ eOutId e
                   , T.pack . showGregorian . utctDay $ eOutDatum e
                   , eOutParzEgrid e
                   , eOutBesKat e
                   , eOutBemerk e
                   , eOutBesitz e
                   , T.pack . show <$> eOutBesId e
                   , eOutBesName e
                   , eOutBesBemerk e
                   , eOutBesOrt e
                   )
