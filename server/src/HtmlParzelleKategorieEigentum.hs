{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlParzelleKategorieEigentum ( katDisplay
                                     , katListDisplay
                                     , katForm) where

import           Data.Maybe        (isJust)
import qualified Data.Text         as T
import           Lucid

import           HtmlGeneric       (selectMultiField,
                                    deleteButton, editButton,
                                    itemDisplay, listDisplay, sTxtInput,
                                    saveButton, withdrawButton)
import           SchemaUser        (User)
import           ViewGeneric       (SfaData, toDisplay)
import           ViewParzelleModel (Kategorie (..))

katListDisplay :: SfaData Kategorie
               => Maybe User -> [Kategorie] -> Html ()
katListDisplay mbUsr keL = listDisplay $ toDisplay mbUsr <$> keL

katDisplay :: Maybe User -> Kategorie -> Html ()
katDisplay mbUser mbKatEigen =
  let object           = "kategorie-eigentum"
      isEditable       = isJust mbUser
      (id', kat)       = serializeKat $ Just mbKatEigen
  in figure_ [ class_ "card display"
             , data_ "id" id'
             , data_ "object" object 
             ] $ do
        figcaption_ [] $ do
          if isEditable
            then div_ [ class_ "register"] $ do
                   div_ [ class_ "edit-buttons" ] $ do
                     editButton
                     deleteButton id' object
            else mempty
        ul_ [] $ do
          itemDisplay "text" Nothing False $ Just kat

katForm :: Maybe Kategorie -> Html ()
katForm mbKatEigen =
  let object           = "kategorie-eigentum"
      (id', kat)       = serializeKat mbKatEigen
  in form_ [ data_ "object" object, data_ "id" id' ] $ do
    figure_ [ class_ "card" ] $ do
      figcaption_ [] $ do
        div_ [ class_ "register"] $ do
          div_ [ class_ "edit-buttons" ] $ do
            saveButton
            withdrawButton
      ul_ [] $ do
        li_ [ data_ "type" "text" ] $ do
          input_ [  name_ "kId"
                  , required_ "true"
                  , value_ id'
                  , type_ "hidden"
                  ]
          sTxtInput True id' "kName" (Just kat)

serializeKat :: Maybe Kategorie -> ( T.Text, T.Text)
serializeKat = maybe ("0", "") (\ke -> (T.pack . show $ kId ke, kName ke))
