{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE TypeOperators     #-}

module DatabaseGebaeude ( infoQuery
                        , qInfoList
                        , qInfoById
                        , qInfoById'
                        , qInfoByEgid
                        , qGebaeudeWithoutEingang
                        , qGebaList
                        , GebaeudeSql
                        ) where

import           Database.Esqueleto.Experimental

import           Data.Int                        (Int64)
import qualified Data.Map.Strict as Map
import           Data.Map.Append  (AppendMap (..))
import           Data.Maybe  (maybeToList)
import qualified Data.Set  as Set
import qualified Data.Text                       as T

import qualified Database                        as Db
import qualified SchemaEingang                   as SchE
import qualified SchemaGebaeude                  as SchG

type GebaeudeSql = ( Entity SchG.Gebaeude
                   , Maybe (Entity SchG.Info)
                   , Maybe (Entity SchG.KategorieInfo)
                   )

type InfoSql = ( Entity SchG.Info
               , Maybe (Entity SchG.KategorieInfo)
               )

newtype InfoInter = InfoInter (Entity SchG.Info
                              , Set.Set (Entity SchG.KategorieInfo)
                              )

type InfoGrouped = (Entity SchG.Info, [Entity SchG.KategorieInfo])


instance Semigroup InfoInter where
  InfoInter (i1, k1) <> InfoInter (i2, k2) = InfoInter (i1, Set.union k1 k2)

type InfoMap = AppendMap SchG.InfoId InfoInter

-- shortcut because used heavily
type InfoL = [(Entity SchG.Info, Entity SchG.KategorieInfo)]

qGebaList :: T.Text -> IO [GebaeudeSql]
qGebaList region = Db.runActionR $
  select $ do
  (g :& i :& _ :& kat) <- gebaQuery
  orderBy [ desc (g ^. SchG.GebaeudeEgid), asc (i ?. SchG.InfoDatum) ]
  groupBy (g ^. SchG.GebaeudeId, i ?. SchG.InfoId, kat ?. SchG.KategorieInfoId)
  pure (g, i,kat)

gebaQuery :: SqlQuery ( SqlExpr (Entity SchG.Gebaeude)
                     :& SqlExpr (Maybe (Entity SchG.Info))
                     :& SqlExpr (Maybe (Entity SchG.InfoKategorie))
                     :& SqlExpr (Maybe (Entity SchG.KategorieInfo))
                      )
gebaQuery = from $ table @SchG.Gebaeude
      `leftJoin` table @SchG.Info
      `on` (\(g :& i) ->
        just (g ^. SchG.GebaeudeEgid) ==. i ?. SchG.InfoGebaeudeEgid)
      `leftJoin` table @SchG.InfoKategorie
      `on` (\(_ :& info_ :& infoKat) ->
        (infoKat ?. SchG.InfoKategorieInfoId) ==. (info_ ?. SchG.InfoId))
      `leftJoin` table @SchG.KategorieInfo
      `on` (\(_ :& infoKat :& kat) ->
        (kat ?. SchG.KategorieInfoId) ==. (infoKat ?. SchG.InfoKategorieKategorieInfoId))

infoQuery :: SqlQuery ( SqlExpr (Entity SchG.Info)
                     :& SqlExpr (Maybe (Entity SchG.InfoKategorie))
                     :& SqlExpr (Maybe (Entity SchG.KategorieInfo))
                      )
infoQuery = from $ table @SchG.Info
              `leftJoin` table @SchG.InfoKategorie
              `on` (\(info_ :& infoKat) ->
                (infoKat ?. SchG.InfoKategorieInfoId) ==. just (info_ ^. SchG.InfoId))
              `leftJoin` table @SchG.KategorieInfo
              `on` (\(_ :& infoKat :& kat) ->
                (kat ?. SchG.KategorieInfoId) ==. (infoKat ?. SchG.InfoKategorieKategorieInfoId))


qInfoList :: IO [InfoGrouped]
qInfoList = fmap processSql (Db.runActionR $
                select $ do
                (info_ :& _ :& kat) <- infoQuery
                orderBy [ desc (info_ ^. SchG.InfoDatum) ]
                pure (info_, kat))

processSql :: [InfoSql] -> [InfoGrouped]
processSql = transformMap . mconcat . groupsToMaps . makeInterGroups

makeInterGroups :: [InfoSql] -> [InfoInter]
makeInterGroups = fmap (\(e,k) ->
                            InfoInter (e,Set.fromList $ maybeToList k))

groupsToMaps :: [InfoInter] -> [InfoMap]
groupsToMaps = fmap groupToMap
  where groupToMap :: InfoInter -> InfoMap
        groupToMap (InfoInter i@(e,_)) =
          AppendMap $ Map.singleton (entityKey e) (InfoInter i)

transformMap :: InfoMap -> [InfoGrouped]
transformMap map = unpackGroup <$> Map.elems (unAppendMap map)
  where unpackGroup (InfoInter (i,k)) = (i,Set.toList k)

qInfoById' :: Int64 -> IO [InfoGrouped]
qInfoById' id = fmap processSql (qInfoById id)

qInfoById :: Int64 -> IO [InfoSql]
qInfoById key = Db.runActionR $
    select $ do
    (info_ :& _ :& kat) <- infoQuery
    where_ ((info_ ^. SchG.InfoId) ==. val (toSqlKey key))
    pure (info_, kat)

qInfoByEgid :: Double -> IO [InfoGrouped]
qInfoByEgid egid = fmap processSql $
  Db.runActionR $
    select $ do
    (info_ :& _ :& kat) <- infoQuery
    where_ (info_ ^. SchG.InfoGebaeudeEgid ==. val egid)
    orderBy [ desc (info_ ^. SchG.InfoDatum) ]
    pure (info_, kat)

qGebaeudeWithoutEingang :: IO [Entity SchG.Gebaeude]
qGebaeudeWithoutEingang = do
  Db.runActionR $
    select $ do
    (geb :& eing) <-
        from $ table @SchG.Gebaeude
        `leftJoin` table @SchE.Eingang
        `on` (\(geb :& eing) ->
          just (geb ^. SchG.GebaeudeEgid) ==. eing ?. SchE.EingangGebaeudeEgid)
    where_ (isNothing $ eing ?. SchE.EingangId)
    pure geb
