{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlGebaeudeKategorieInfo ( katInfoDisplay
                                 , katInfoListDisplay
                                 , katInfoForm) where

import           Data.Maybe        (isJust)
import qualified Data.Text         as T
import           Lucid

import           HtmlGeneric       (deleteButton, editButton,
                                    itemDisplay, listDisplay, sTxtInput,
                                    saveButton, withdrawButton)
import           SchemaUser        (User)
import           ViewGebaeudeModel (Kategorie (..))
import           ViewGeneric       (SfaData, toDisplay)

katInfoListDisplay :: SfaData Kategorie
                   => Maybe User -> [Kategorie] -> Html ()
katInfoListDisplay mbUsr kiL = listDisplay $ toDisplay mbUsr <$> kiL

katInfoDisplay :: Maybe User -> Kategorie -> Html ()
katInfoDisplay mbUser mbKatInfo =
  let object           = "kategorie-info"
      isEditable       = isJust mbUser
      (id', kat)       = serializeKatInfo $ Just mbKatInfo
  in figure_ [ class_ "card display"
             , data_ "id" id'
             , data_ "object" object 
             ] $ do
      figcaption_ [] $ do
        div_ [ class_ "register"] $ do
          if isEditable
            then div_ [ class_ "edit-buttons" ] $ do
              editButton
              deleteButton id' object
            else mempty
      ul_ [] $ do
        itemDisplay "str" Nothing False $ Just kat

katInfoForm :: Maybe Kategorie -> Html ()
katInfoForm mbKatInfo =
  let object           = "kategorie-info"
      (id', kat)       = serializeKatInfo mbKatInfo
  in form_ [ data_ "object" object, data_ "id" id' ] $ do
    figure_ [ class_ "card" ] $ do
      figcaption_ [] $ do
        div_ [ class_ "register"] $ do
          div_ [ class_ "edit-buttons" ] $ do
            saveButton
            withdrawButton
      ul_ [] $ do
        input_ [  name_ "kId"
                , required_ "true"
                , value_ id'
                , type_ "hidden"
                ]
        li_ [ data_ "type" "text" ] $ do
          sTxtInput True id' "kName" (Just kat)

serializeKatInfo :: Maybe Kategorie -> ( T.Text, T.Text)
serializeKatInfo = maybe ("0", "") (\ki -> (T.pack . show $ kId ki, kName ki))
