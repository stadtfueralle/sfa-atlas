{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StandaloneDeriving    #-}
-- Using geos to convert postgresql BytesStrings to Haskell types and vice
-- versa, mainly stolen from https://github.com/polimorphic/persistent-postgis
-- which is not published on Hackage.

module PostgisHaskellBinding where

import           Data.Aeson
import           Data.Geometry.Geos.Geometry          (Coordinate (..),
                                                       Geometry (MultiPolygonGeometry, PointGeometry, PolygonGeometry),
                                                       LinearRing (..),
                                                       MultiPolygon (..),
                                                       Point (..), Polygon (..),
                                                       Some (Some))
import           Data.Geometry.Geos.Serialize         (readHex, writeHex)
import qualified Data.Vector                          as V
import           Database.Persist.Class
import           Database.Persist.Sql

import qualified Database.Esqueleto.Experimental      as Esq (SqlExpr, Value)
import           Database.Esqueleto.Internal.Internal (unsafeSqlFunction)


data Geo a where
  GeoPoint :: Point -> Geo Point
  GeoPolygon :: Polygon -> Geo Polygon
  GeoMultiPolygon :: MultiPolygon -> Geo MultiPolygon

deriving instance Eq (Geo a)
deriving instance Show (Geo a)
deriving instance Show (Some Geo)

instance ToJSON (Geo a) where
    toJSON (GeoPoint p) =
       object [ "type" .= ("Point" :: String)
              , "coordinates" .= pointToList p
              ]
    toJSON (GeoPolygon pl) =
        object [ "type" .= ("Polygon" :: String)
               , "coordinates" .= pgToList pl
               ]
    toJSON (GeoMultiPolygon mpl) =
        object [ "type" .= ("MultiPolygon" :: String)
               , "coordinates" .= ((((roundMuPo <$>) <$>) <$>) <$> mpToList mpl)
               ]
      where
        roundMuPo :: Double -> Double
        roundMuPo mp = fromIntegral (floor (mp * 10^5)) / 10^5

instance Eq (Some Geo) where
    Some (GeoPoint a)        == Some (GeoPoint b)        = a == b
    Some (GeoPolygon a)      == Some (GeoPolygon b)      = a == b
    Some (GeoMultiPolygon a) == Some (GeoMultiPolygon b) = a == b
    _ == _                                               = False

instance ToJSON (Some Geo) where
    toJSON (Some g) = toJSON g


-- ! instances for persist-postgresql
instance PersistField (Geo Point) where
    toPersistValue (GeoPoint p) = PersistLiteralEscaped . writeHex $
      PointGeometry p (Just 4326)
    fromPersistValue (PersistLiteralEscaped b) = case readHex b of
      Just (Some (PointGeometry p (Just 4326))) ->
        Right $ GeoPoint p
      _                                         ->
        Left "Invalid Geography Point hex"
    fromPersistValue _ = Left "Geography values must be converted from PersistLiteralEscaped"

instance PersistField (Geo Polygon) where
    toPersistValue (GeoPolygon p) = PersistLiteralEscaped . writeHex $
      PolygonGeometry p (Just 4326)
    fromPersistValue (PersistLiteralEscaped b) = case readHex b of
      Just (Some (PolygonGeometry p (Just 4326))) ->
        Right $ GeoPolygon p
      _                                                ->
        Left "Invalid Geography Polygon hex"
    fromPersistValue _ = Left "Geography values must be converted from PersistLiteralEscaped"

instance PersistField (Geo MultiPolygon) where
    toPersistValue (GeoMultiPolygon p) = PersistLiteralEscaped . writeHex $
      MultiPolygonGeometry p (Just 4326)
    fromPersistValue (PersistLiteralEscaped b) = case readHex b of
      Just (Some (MultiPolygonGeometry p (Just 4326))) ->
        Right $ GeoMultiPolygon p
      _                                                ->
        Left "Invalid Geography MultiPolygon hex"
    fromPersistValue _ = Left "Geography values must be converted from PersistLiteralEscaped"

instance PersistFieldSql (Geo Point) where
  sqlType _ = SqlOther "geometry(Point,4326)"

instance PersistFieldSql (Geo Polygon) where
  sqlType _ = SqlOther "geometry(Polygon,4326)"

instance PersistFieldSql (Geo MultiPolygon) where
  sqlType _ = SqlOther "geometry(MultiPolygon,4326)"

dismanlteGeoMultiPg :: Geo MultiPolygon -> V.Vector (V.Vector (V.Vector (V.Vector Double)))
dismanlteGeoMultiPg (GeoMultiPolygon mPg) = dismantleMultiPg mPg

dismantleMultiPg :: MultiPolygon -> V.Vector (V.Vector (V.Vector (V.Vector Double)))
dismantleMultiPg mPg = dismantlePg <$> peelMultiPg mPg

dismantleLr :: LinearRing -> V.Vector (V.Vector Double)
dismantleLr lR = peelCoord <$> peelLr lR

peelPg :: Polygon -> V.Vector LinearRing
peelPg (Polygon lrV) = lrV

peelPg' :: Polygon -> V.Vector (V.Vector Coordinate)
peelPg' (Polygon lrV) = coordinateSequenceLinearRing <$> lrV


peelLr :: LinearRing -> V.Vector Coordinate
peelLr = coordinateSequenceLinearRing

class IsGeography g
instance IsGeography (Geo a)
instance IsGeography (Some Geo)
instance IsGeography a => IsGeography (Maybe a)

contains :: (IsGeography a, IsGeography b)
         => Esq.SqlExpr (Esq.Value a)
         -> Esq.SqlExpr (Esq.Value b)
         -> Esq.SqlExpr (Esq.Value Bool)
contains a b = unsafeSqlFunction "ST_Contains" (a, b)
infixl 7 `contains`

intersects :: (IsGeography a, IsGeography b)
  => Esq.SqlExpr (Esq.Value a)
  -> Esq.SqlExpr (Esq.Value b)
  -> Esq.SqlExpr (Esq.Value Bool)
intersects a b = unsafeSqlFunction "ST_Intersects" (a, b)
infixl 7 `intersects`

peelCoord :: Coordinate -> V.Vector Double
peelCoord (Coordinate2 x y)   = V.fromList [x,y]
peelCoord (Coordinate3 x y _) = V.fromList [x,y]

peelCoord' :: Coordinate -> [Double]
peelCoord' (Coordinate2 x y)   = [x,y]
peelCoord' (Coordinate3 x y _) = [x,y]

pgToList :: Polygon -> [[[Double]]]
pgToList pg = V.toList <$> V.toList (dismantlePg' pg)

pointToList :: Point -> [Double]
pointToList (Point coord) = peelCoord' coord

dismantlePg :: Polygon -> V.Vector (V.Vector (V.Vector Double))
dismantlePg pg = dismantleLr <$> peelPg pg

dismantlePg' :: Polygon -> V.Vector (V.Vector [Double])
dismantlePg' pg = fmap peelCoord' <$> peelPg' pg

mpToList :: MultiPolygon -> [[[[Double]]]]
mpToList mp = pgToList <$> V.toList (peelMultiPg mp)

peelMultiPg :: MultiPolygon -> V.Vector Polygon
peelMultiPg (MultiPolygon pgV) = pgV
