{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE TypeOperators     #-}

module DatabaseParzelle ( qParzelleByEgrid
                        , qEigentumByEgrid
                        , qEigentumList
                        , qEigentumById
                        , qEigentumById'
                        , qBesitzendeById
                        , qBesitzendeList
                        , qParzList
                        , ParzelleSql
                        ) where

import  Data.List (sortBy)
import qualified Data.Map.Strict as Map
import           Data.Map  (Map (..), singleton, fromList)
import           Data.Map.Append  (AppendMap (..))
import           Data.Maybe  (maybeToList)
import qualified Data.Function as F
import           Data.Semigroup 
import           Data.Set  (Set)
import qualified Data.Set  as Set
import qualified Data.Text                       as T
import           Data.Int                        (Int64)
import qualified Database.Persist.Sql            as Sql
import           Database.Esqueleto.Experimental

import qualified Database                        as Db
import qualified SchemaParzelle                  as SchP


-- one parzelle row left joined with Eigentum & co.
type ParzelleSql = ( Entity SchP.Parzelle
                   , Maybe (Entity SchP.Eigentum)
                   , Maybe (Entity SchP.Besitzende)
                   , Maybe (Entity SchP.Kategorie)
                   )

-- We have to deal with the n to m relation between Eigentum and
-- Kategorie.
-- Instead of the 2 dimensional row-by-row sql representation of a n to m
-- join, we need to group all rows with the same Eigentum, as explained here:
-- https://www.foxhound.systems/blog/grouping-query-results-haskell/
--
-- [EigentumSql] -> [EigentumInter] -> [EigentumMap] -> EigentumMap
-- -> [EingentumGrouped]

type BesitzendeSql = (Entity SchP.Besitzende, Maybe (Entity SchP.Kategorie))
newtype BesitzendeInter = BesitzendeInter ( Entity SchP.Besitzende
                                          , Set (Entity SchP.Kategorie)
                                          )
type BesitzendeGrouped = (Entity SchP.Besitzende, [Entity SchP.Kategorie])
type BesitzendeMap = AppendMap SchP.BesitzendeId BesitzendeInter

instance Semigroup BesitzendeInter where
  BesitzendeInter (b1, k1) <> BesitzendeInter (b2, k2) =
     BesitzendeInter (b1, Set.union k1 k2)

type EigentumSql = ( Entity SchP.Eigentum
                   , Maybe (Entity SchP.Besitzende)
                   , Maybe (Entity SchP.Kategorie)
                   )
newtype EigentumInter = EigentumInter ( Entity SchP.Eigentum
                                      , Maybe (Entity SchP.Besitzende)
                                      , Set (Entity SchP.Kategorie)
                                      )
type EigentumGrouped = ( Entity SchP.Eigentum
                       , Maybe (Entity SchP.Besitzende)
                       , [Entity SchP.Kategorie]
                       )

instance Semigroup EigentumInter where
  EigentumInter (e1, b1, k1) <> EigentumInter (e2, b2, k2) =
     EigentumInter (e1, b1, Set.union k1 k2)

type EigentumMap = AppendMap SchP.EigentumId EigentumInter

besSqlToGroup :: [BesitzendeSql] -> [BesitzendeGrouped]
besSqlToGroup = sortByName . besTransform . mconcat . besMaps . besInterGroups
 where
   besInterGroups :: [BesitzendeSql] -> [BesitzendeInter]
   besInterGroups = fmap (\(b,k) ->
                               BesitzendeInter (b,Set.fromList $ maybeToList k))
   besMaps :: [BesitzendeInter] -> [BesitzendeMap]
   besMaps = fmap groupToMap
     where groupToMap :: BesitzendeInter -> BesitzendeMap
           groupToMap (BesitzendeInter bes@(b,_)) =
             AppendMap $ Map.singleton (entityKey b) (BesitzendeInter bes)
   besTransform :: BesitzendeMap -> [BesitzendeGrouped]
   besTransform map = unpackGroup <$> Map.elems (unAppendMap map)
     where unpackGroup (BesitzendeInter (b,k)) = (b,Set.toList k)
   sortByName :: [BesitzendeGrouped] -> [BesitzendeGrouped]
   sortByName = sortBy (compare `F.on` (Sql.entityVal . fst))

processSql :: [EigentumSql] -> [EigentumGrouped]
processSql = transformMap . mconcat . groupsToMaps . makeInterGroups
  where
    makeInterGroups :: [EigentumSql] -> [EigentumInter]
    makeInterGroups = fmap (\(e,b,k) ->
                                EigentumInter (e,b,Set.fromList $ maybeToList k))
    
    groupsToMaps :: [EigentumInter] -> [EigentumMap]
    groupsToMaps = fmap groupToMap
      where groupToMap :: EigentumInter -> EigentumMap
            groupToMap (EigentumInter ei@(e,_,_)) =
              AppendMap $ Map.singleton (entityKey e) (EigentumInter ei)
    
    transformMap :: EigentumMap -> [EigentumGrouped]
    transformMap map = unpackGroup <$> Map.elems (unAppendMap map)
      where unpackGroup (EigentumInter (e,b,k)) = (e,b,Set.toList k)

qBesitzendeList :: IO [BesitzendeGrouped]
qBesitzendeList = do
    besSql <- Db.runActionR $
                    select $ do
                      (bes :& _ :& kat) <- besQuery
                      -- has no effect, will be discarded in besSqlToGroup 
                      -- orderBy [ asc (bes ^. SchP.BesitzendeName) ]
                      pure (bes, kat)
    -- print $ take 50 $ SchP.besitzendeName . Sql.entityVal . fst <$> besSql
    pure $ besSqlToGroup besSql 

qBesitzendeById :: Int64 -> IO [BesitzendeGrouped]
qBesitzendeById key = fmap besSqlToGroup (qBesitzendeByIdRaw key)

qBesitzendeByIdRaw :: Int64 -> IO [BesitzendeSql]
qBesitzendeByIdRaw key = Db.runActionR $
    select $ do
    (bes :& _ :& kat) <- besQuery
    where_ ((bes ^. SchP.BesitzendeId) ==. val (toSqlKey key))
    pure (bes, kat)

besQuery :: SqlQuery ( SqlExpr (Entity SchP.Besitzende)
                     :& SqlExpr (Maybe (Entity SchP.BesitzendeKategorie))
                     :& SqlExpr (Maybe (Entity SchP.Kategorie))
                     )
besQuery = from $ table @SchP.Besitzende
      `leftJoin` table @SchP.BesitzendeKategorie
      `on` (\(bes :& besKat) ->
        (besKat ?. SchP.BesitzendeKategorieBesitzendeId) ==. just (bes ^. SchP.BesitzendeId))
      `leftJoin` table @SchP.Kategorie
      `on` (\(_ :& besKat :& kat) ->
        (kat ?. SchP.KategorieId) ==. (besKat ?. SchP.BesitzendeKategorieKategorieId))

parzQuery :: SqlQuery (  SqlExpr (Entity SchP.Parzelle)
                      :& SqlExpr (Maybe (Entity SchP.Eigentum))
                      :& SqlExpr (Maybe (Entity SchP.Besitzende))
                      :& SqlExpr (Maybe (Entity SchP.BesitzendeKategorie))
                      :& SqlExpr (Maybe (Entity SchP.Kategorie))
                      )
parzQuery = from $ table @SchP.Parzelle
      `leftJoin` table @SchP.Eigentum
      `on` (\(p :& e) ->
        just (p ^. SchP.ParzelleEgrid) ==. e ?. SchP.EigentumParzelleEgrid)
      `leftJoin` table @SchP.Besitzende
      `on` (\(_ :& e :& bes) ->
        just (bes ?. SchP.BesitzendeId) ==. e ?. SchP.EigentumBesitzende)
      `leftJoin` table @SchP.BesitzendeKategorie
      `on` (\(_ :& _ :& bes :& besKat) ->
        (besKat ?. SchP.BesitzendeKategorieBesitzendeId) ==. (bes ?. SchP.BesitzendeId))
      `leftJoin` table @SchP.Kategorie
      `on` (\(_ :& _ :& besKat :& kat) ->
        (kat ?. SchP.KategorieId) ==. (besKat ?. SchP.BesitzendeKategorieKategorieId))

eigenQuery :: SqlQuery ( SqlExpr (Entity SchP.Eigentum)
                       :& SqlExpr (Maybe (Entity SchP.Besitzende))
                       :& SqlExpr (Maybe (Entity SchP.BesitzendeKategorie))
                       :& SqlExpr (Maybe (Entity SchP.Kategorie))
                       )
eigenQuery = from $ table @SchP.Eigentum
      `leftJoin` table @SchP.Besitzende
      `on` (\(eigen :& bes) ->
        bes ?. SchP.BesitzendeId ==. eigen ^. SchP.EigentumBesitzende)
      `leftJoin` table @SchP.BesitzendeKategorie
      `on` (\(_ :& bes :& besKat) ->
        (besKat ?. SchP.BesitzendeKategorieBesitzendeId) ==. (bes ?. SchP.BesitzendeId))
      `leftJoin` table @SchP.Kategorie
      `on` (\(_ :& _ :& besKat :& kat) ->
        (kat ?. SchP.KategorieId) ==. (besKat ?. SchP.BesitzendeKategorieKategorieId))

qEigentumList :: IO [EigentumGrouped]
qEigentumList = fmap processSql (Db.runActionR $
                    select $ do
                    (eigen :& besitz :& _ :& kat) <- eigenQuery
                    orderBy [ desc (eigen ^. SchP.EigentumDatum) ]
                    groupBy (eigen ^. SchP.EigentumId, besitz ?. SchP.BesitzendeId, kat ?. SchP.KategorieId)
                    pure (eigen, besitz, kat))

-- TODO implement schema for Wohnviertel and use 'contains' function from
-- PostgisHaskellBinding to filter parzelle of specific viertel
qParzList :: T.Text -> T.Text -> IO [ParzelleSql]
qParzList typ region = Db.runActionR $
  select $ do
  (p :& e :& b :& _ :& kat) <- parzQuery
  where_ $ ((p ^. SchP.ParzelleTyp) `like` val typ) &&. ((p ^. SchP.ParzelleArt) `like` val typ)
  orderBy [ desc (p ^. SchP.ParzelleEgrid), asc (e ?. SchP.EigentumDatum) ]
  groupBy (p ^. SchP.ParzelleId, e ?. SchP.EigentumId, b ?. SchP.BesitzendeId, kat ?. SchP.KategorieId)
  pure (p, e, b, kat)

qEigentumById :: Int64 -> IO [EigentumSql]
qEigentumById key = Db.runActionR $
    select $ do
    (eigen :& besitz :& _ :& kat) <- eigenQuery
    where_ ((eigen ^. SchP.EigentumId) ==. val (toSqlKey key))
    pure (eigen, besitz, kat)

qEigentumById' :: Int64 -> IO [EigentumGrouped]
qEigentumById' id = fmap processSql (qEigentumById id)


qEigentumByEgrid :: T.Text -> IO [EigentumGrouped]
qEigentumByEgrid egrid = fmap processSql $
  Db.runActionR $
    select $ do
    (eigen :& besitz :& _ :& kat) <- eigenQuery
    where_ (eigen ^. SchP.EigentumParzelleEgrid ==. val egrid)
    orderBy [ desc (eigen ^. SchP.EigentumDatum) ]
    pure (eigen, besitz, kat)


qParzelleByEgrid :: T.Text -> IO (Maybe (Entity SchP.Parzelle))
qParzelleByEgrid egrid = do
  Db.runActionR (getBy $ SchP.UniqueEgrid egrid)
