{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}

module Server where

import           Control.Monad.IO.Class     (liftIO)
import qualified Data.ByteString.Lazy       as Lazy
import           Data.Proxy                 (Proxy (..))
import           Network.HTTP.Media         ((//), (/:))
import           Network.Wai.Handler.Warp   (run)
import           Servant                    (Accept (..), Get, Header, Headers,
                                             MimeRender (..), addHeader)
import           Servant.API                (Raw, (:<|>) (..), (:>))
import           Servant.Auth.Server        (Cookie, CookieSettings,
                                             IsSecure (NotSecure, Secure),
                                             JWTSettings, SameSite (..),
                                             cookieIsSecure, cookieSameSite,
                                             cookieXsrfSetting,
                                             defaultCookieSettings,
                                             defaultJWTSettings, generateKey)
import           Servant.Server             (Context (EmptyContext, (:.)),
                                             Server, serveWithContext)
import           Servant.Server.StaticFiles (serveDirectoryWebApp)
import           System.Environment         (getEnv)

import qualified ApiBase                    as Basic
import qualified Auth
import qualified GeoBsMap                   as Map
import qualified OIDCClient
import qualified RouteGebaeude              as RouteG
import qualified RouteParzelle              as RouteP

type API auths = "api"      :> Basic.API auths
            :<|> "static"   :> Raw
            :<|> "auth"     :> Auth.API auths
            :<|> Map.API auths
            :<|> "oidc"     :> OIDCClient.API
            :<|> RouteG.API auths
            :<|> RouteP.API auths
            :<|> "sw" :> Get '[JS] (Headers '[Header "Service-Worker-Allowed" String] RawJS)

-- service worker js needs to be served with special headers for scope tuning
data JS
newtype RawJS = RawJS { unRaw :: Lazy.ByteString }

instance Accept JS where
  contentType _ = "text" // "javascript" /: ("charset", "utf-8")

instance MimeRender JS RawJS where
  mimeRender _ = unRaw

readSW :: IO (Headers '[Header "Service-Worker-Allowed" String] RawJS)
readSW = do
  swFilePath <- getEnv "SW_FILE_PATH"
  addHeader "/" . RawJS <$> Lazy.readFile swFilePath


data Config = Config { envir    :: String
                     , whatever :: String
                     }

server :: CookieSettings
       -> JWTSettings
       -> Config
       -> Server (API auths)
server cookieSettings jwtCfg conf = Basic.server
                               :<|> serveDirectoryWebApp "static"
                               :<|> Auth.server cookieSettings jwtCfg
                               :<|> Map.server
                               :<|> OIDCClient.handlers cookieSettings jwtCfg
                               :<|> RouteG.server
                               :<|> RouteP.server
                               :<|> liftIO readSW

authCookieSettings :: IO CookieSettings
authCookieSettings = do
  env <- getEnv "ENVIRONMENT"
  if env == "development"
  then pure $ defaultCookieSettings
    { cookieIsSecure = NotSecure -- cookies über http schicken, nur für local dev Umgebung!
    , cookieSameSite = SameSiteLax -- for prod change to SameSiteStrict
    , cookieXsrfSetting = Nothing
    }
  else pure $ defaultCookieSettings
    { cookieIsSecure = Secure
    , cookieSameSite = SameSiteStrict
    , cookieXsrfSetting = Nothing
    }

runServer :: IO ()
runServer = do
  -- We *also* need a key to sign the cookies
  myKey <- generateKey
  cookieSettings <- authCookieSettings
  -- Adding some configurations. 'Cookie' requires, in addition to
  -- CookieSettings, JWTSettings (for signing), so everything is just as before
  let jwtCfg = defaultJWTSettings myKey
      cfg = cookieSettings :. jwtCfg :. EmptyContext
      api = Proxy :: Proxy (API '[Cookie])
  run 8000 $ serveWithContext api cfg (server cookieSettings jwtCfg (Config { envir = "dev" }))
