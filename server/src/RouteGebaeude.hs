{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeApplications      #-}


module RouteGebaeude where

import           Servant             (Capture, Get, Server, (:<|>) (..), (:>))
import           Servant.Auth.Server (Auth, AuthResult (Authenticated),
                                      throwAll)

import           CommonLucid         (HTML, RawHtml (..))
import           HttpErrors          (unauthorized)
import           SchemaUser          (User)
import           ViewGebaeude        (gebByEgid, katChooserServer)
import           ViewGebaeudeModel   (InfoShow, InfoSave, Kategorie)
import           ViewGeneric         (AppRoute, ForeignRelRoute, SaveRoute, SnippetRoute,
                                      appServer, saveServer, snippetServer)

type InfoRoute = AppRoute InfoShow
            :<|> SnippetRoute InfoShow
            :<|> SaveRoute InfoSave
            :<|> "inKatIdList" :> ForeignRelRoute

infoServer :: User -> Server InfoRoute
infoServer usr = appServer @InfoShow usr
            :<|> snippetServer @InfoShow usr
            :<|> saveServer usr
            :<|> katChooserServer usr

type KatInfoRoute = AppRoute Kategorie
               :<|> SaveRoute Kategorie
               :<|> SnippetRoute Kategorie

katInfoServer :: User -> Server KatInfoRoute
katInfoServer usr = appServer @Kategorie usr 
               :<|> saveServer usr
               :<|> snippetServer @Kategorie usr

type Route = "info"           :> InfoRoute
        :<|> "kategorie-info" :> KatInfoRoute
        :<|> "gebaeudeByEgid" :> Capture "egid" Double
                              :> Get '[HTML] RawHtml

type API auths = Auth auths User :> Route

server :: AuthResult User -> Server Route
server (Authenticated usr) = infoServer usr
                        :<|> katInfoServer usr 
                        :<|> gebByEgid (Just usr)
server _                   = throwAll unauthorized 
                        :<|> throwAll unauthorized
                        :<|> gebByEgid Nothing
