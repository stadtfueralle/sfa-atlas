{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module SchemaGebaeude where

import           Data.Aeson                  (FromJSON, ToJSON, object, toJSON,
                                              (.=))
import           Data.Geometry.Geos.Geometry (MultiPolygon (..), Some (Some))
import qualified Data.Text                   as T
import           Data.Time                   (UTCTime)
import qualified Database.Persist.TH         as PTH
import           GHC.Generics                (Generic)
import qualified Database                    as Db

import qualified ApiGeneric
import           PostgisHaskellBinding       (Geo)

PTH.share [PTH.mkPersist PTH.sqlSettings, PTH.mkMigrate "migrateAll"] [PTH.persistLowerCase|
Gebaeude sql=gebaeude
  geom (Geo MultiPolygon)
  simpleGeom (Geo MultiPolygon) generated=ST_Simplify(geom,0.00001,true)
  egid Double
  status T.Text
  egrid T.Text
  baujahr Int Maybe
  abbruchjahr Int Maybe
  flaeche Int Maybe
  volumen Int Maybe
  bezeich T.Text Maybe
  kategor T.Text Maybe
  anzahlgeschosse Int Maybe
  anzahlwohnung Int Maybe
  created UTCTime default=CURRENT_TIMESTAMP
  UniqueEgid egid
  deriving Generic Show

KategorieInfo sql=kategorie_info
  kategorie T.Text
  UniqueKategorieInfo kategorie
  deriving Show Generic ToJSON FromJSON

Info sql=info
  datum UTCTime default=CURRENT_TIMESTAMP
  gebaeudeEgid Double
  info T.Text
  absender T.Text Maybe
  Foreign Gebaeude fk_info_gebaeude gebaeudeEgid References egid
  UniqueInfo datum gebaeudeEgid
  deriving Show Generic ToJSON FromJSON

InfoKategorie sql=info_kategorie
  infoId InfoId OnDeleteSetDefault default=1
  kategorieInfoId KategorieInfoId OnDeleteCascade OnUpdateCascade
  UniqueInfoKategorie infoId kategorieInfoId

|]

instance Eq KategorieInfo where
  (KategorieInfo name1) == (KategorieInfo name2) =
                  name1 == name2
 
instance Ord KategorieInfo where
  (KategorieInfo name1) `compare` (KategorieInfo name2) =
                  name1 `compare` name2

instance Db.SqlClass Info
instance Db.SqlClass KategorieInfo
