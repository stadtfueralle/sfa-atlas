{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE OverloadedStrings #-}

module ViewGebaeude ( gebByEgid
                    , katChooserServer
                    ) where

import           Control.Monad.IO.Class          (liftIO)
import           Control.Monad.Trans.Except      (throwE)
import           Data.Int                        (Int64)
import           Data.List                       ((\\), partition)
import           Data.Maybe                      (maybeToList, fromMaybe)
import           Database.Esqueleto.Experimental
import qualified Data.Text                       as T
import           Data.Time                       (getCurrentTime, UTCTime (..))
import           Lucid                           (renderBS, Html (..))
import           Servant                         (Handler (..), err401, errBody)


import           CommonLucid                     (RawHtml (..))
import qualified Database                        as Db
import qualified DatabaseGebaeude                as DbGeb
import qualified DbRooseli                       as DbR
import           HtmlGebaeude                    (gebaeudeDisplay)
import           HtmlGebaeudeInfo                (infoDisplay, infoForm,
                                                  infoListDisplay)
import           HtmlGebaeudeKategorieInfo       (katInfoDisplay, katInfoForm,
                                                  katInfoListDisplay)
import           HtmlGeneric                     (selectDialog)
import qualified SchemaGebaeude                  as SchG
import           SchemaUser                      (User)
import           ViewGebaeudeModel               (InfoSave (..), InfoShow (..), Kategorie (..))
import           ViewGeneric                     (Save (..), SfaData (..), SqlSchema (..))

instance SfaData InfoShow where
  name        = "info"
  getById id' = fmap (assembleInfo <$>) (DbGeb.qInfoById' id')
  getList     = fmap (assembleInfo <$>) DbGeb.qInfoList
  emptySfa    = do
    now <- getCurrentTime
    pure $ InfoShow 0 now 0 "" Nothing []
  toFormSfa   = infoForm
  toDisplay   = infoDisplay
  toList      = infoListDisplay

instance Save InfoSave where
  toSqlRow' (InfoSave _ dat egid txt abs katL) =
    SqlInfo $ SchG.Info (UTCTime dat 0) egid txt abs
  screate usr e = do
    info <- liftIO $ processInfoSave (Db.insertUnique . getInfo . toSqlRow') e
    pure $ toDisplay  (Just usr) info
  supdate usr e = do
    info <- liftIO $ processInfoSave (Db.updateRecord' (inId e) . getInfo . toSqlRow') e
    pure $ toDisplay  (Just usr) info

instance Save Kategorie where
  toSqlRow' (Kategorie _ name) =
    SqlKategorieInfo $ SchG.KategorieInfo name
  screate usr k = do
    reco <- (Db.insertUniqueRawRec . getIKat . toSqlRow') k
    pure $ toDisplay  (Just usr) $
      fromSql (fromSqlKey $ entityKey reco) (entityVal reco)
  supdate usr k = do
    katId <- liftIO $ (Db.updateRecord' (kId k) . getIKat . toSqlRow') k
    recList <- getById katId :: IO [Kategorie]
    emptyKat <- emptySfa @Kategorie
    if null recList
    then pure $ toDisplay (Just usr) emptyKat
    else pure $ toDisplay (Just usr) (head recList)

instance SfaData Kategorie where
  name        = "kategorie-info"
  getById id'  = do
    mbRec <- (Db.runActionR . get . toSqlKey) id'
    pure $ fromSql id' <$> maybeToList mbRec
  getList      = fmap (uncurry fromSql <$>) Db.qRecordList'
  emptySfa     = pure $ Kategorie 0 ""
  toFormSfa    = katInfoForm . Just
  toDisplay    = katInfoDisplay
  toList       = katInfoListDisplay

fromSql id' k = Kategorie id' (SchG.kategorieInfoKategorie k)

processInfoSave :: (InfoSave -> IO Int64) -> InfoSave -> IO InfoShow
processInfoSave saveFunc info = do
  currentKats <- getOldIkat $ inId info
  print currentKats
  print $ inKatIdList info
  objId <- saveFunc info
  createEntries objId (inKatIdList info \\  currentKats)
  deleteEntries objId (currentKats \\ inKatIdList info)
  newId <- getById objId
  if null newId -- TODO head is unsafe, replace with error handling
  then head <$> getById (inId info)
  else pure $ head newId
  where 
    getOldIkat :: Int64 -> IO [Int64]
    getOldIkat id' = do
      entities <- Db.runActionR . select $ do
        kat <- from $ table @SchG.InfoKategorie
        where_ (kat ^. SchG.InfoKategorieInfoId ==. val (toSqlKey id'))
        pure kat
      pure $ peelKatId <$> entities
    createEntries :: Int64 -> [Int64] -> IO ()
    createEntries infoId katIdList = liftIO . Db.runActionR $
      insertMany_ (fmap (formEntry infoId) katIdList)
    deleteEntries :: Int64 -> [Int64] -> IO ()
    deleteEntries infoId katIdList = liftIO . Db.runActionR . delete $ do
      kat <- from $ table @SchG.InfoKategorie
      where_ $ kat ^. SchG.InfoKategorieInfoId ==. val (toSqlKey infoId)
      where_ $ kat ^. SchG.InfoKategorieKategorieInfoId
               `in_` valList (toSqlKey <$> katIdList)
    formEntry :: Int64 -> Int64 -> SchG.InfoKategorie
    formEntry iId katId = SchG.InfoKategorie (toSqlKey iId) (toSqlKey katId)
    peelKatId :: Entity SchG.InfoKategorie -> Int64
    peelKatId = fromSqlKey . SchG.infoKategorieKategorieInfoId . entityVal

katChooserServer :: User -> [Int64] -> Handler RawHtml
katChooserServer usr idList = do
  allKat <- liftIO getList
  liftIO . pure . RawHtml . renderBS $ selectKatDialog idList allKat
  where
    selectKatDialog :: [Int64] -> [Kategorie] -> Html ()
    selectKatDialog selectedIds all =
      let (selected, available) = partition (checkMember selectedIds) all
      in  selectDialog "inKatIdList" (formKat <$> selected) 
                                        (formKat <$> available)
    checkMember :: [Int64] -> Kategorie -> Bool
    checkMember idL ke = kId ke `elem` idL
    formKat :: Kategorie -> (T.Text, T.Text)
    formKat ke = (T.pack . show $ kId ke, kName ke)

assembleInfo :: (Entity SchG.Info , [Entity SchG.KategorieInfo]) -> InfoShow
assembleInfo (Entity iKey i, katL) = InfoShow (fromSqlKey iKey)
                                                  (SchG.infoDatum i)
                                                  (SchG.infoGebaeudeEgid i)
                                                  (SchG.infoInfo i)
                                                  (SchG.infoAbsender i)
                                                  (handleList <$> katL)
  where handleList (Entity katKey kat) = ( fromSqlKey katKey
                                         , SchG.kategorieInfoKategorie kat
                                         )

handleGebaeude :: Maybe User -> SchG.Gebaeude -> Handler RawHtml
handleGebaeude mbUsr g = do
  eingangL <- liftIO . Db.peelValues $ DbR.qEingangByGebaeude g
  infoL    <- liftIO $ (assembleInfo <$>) <$> DbGeb.qInfoByEgid (SchG.gebaeudeEgid g)
  pure . RawHtml . renderBS $ gebaeudeDisplay mbUsr g infoL eingangL

gebByEgid :: Maybe User -> Double -> Handler RawHtml
gebByEgid mbUsr egid = do
  mbGeb <- liftIO $ DbR.qGebaeudeByEgid egid
  case mbGeb of
    Just (Entity _ val') -> handleGebaeude mbUsr val'
    Nothing              -> Handler err
  where
    err = throwE $ err401 { errBody = "Could not find Gebaeude with that egid" }
