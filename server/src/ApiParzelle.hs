{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module ApiParzelle where

import           Servant             (Server, err401, (:<|>) (..), (:>))

import qualified ApiGeneric          as Generic
import           SchemaParzelle      (Besitzende, Eigentum, Kategorie)
import           SchemaUser          (User)
import           Servant.Auth.Server (Auth, AuthResult (Authenticated),
                                      throwAll)

type OpenApi auths =
      "eigentum"           :> Generic.API Eigentum auths
 :<|> "kategorie-eigentum" :> Generic.API Kategorie auths

type ProtectedApi auths =
      "besitzende"        :> Generic.API Besitzende auths

type API auths = Auth auths User :> (ProtectedApi auths :<|> OpenApi auths)

protected :: AuthResult User -> Server (ProtectedApi auths)
protected (Authenticated _) = Generic.server
protected                 _ = throwAll err401

open :: Server (OpenApi auths)
open = Generic.server :<|> Generic.server

server :: Server (API auths)
server authUsr = protected authUsr :<|> open

