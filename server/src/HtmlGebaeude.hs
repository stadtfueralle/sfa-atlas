{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}


module HtmlGebaeude where

import           Control.Monad.Identity
import           Data.Double.Conversion.Text (toFixed)
import qualified Data.Text                   as T
import           Lucid
import           Lucid.Base                  (HtmlT (HtmlT))

import           HtmlGeneric                 (itemDisplay, newButton)
import           SchemaEingang               (Eingang (..))
import qualified SchemaGebaeude              as SchG
import           SchemaUser                  (User)
import           ViewGebaeudeModel           (InfoShow)
import           ViewGeneric                 (SfaData, toList)

gebaeudeDisplay :: SfaData InfoShow => Maybe User
                 -> SchG.Gebaeude
                 -> [InfoShow]
                 -> [Eingang]
                 -> Html ()
gebaeudeDisplay mbUsr g iList eList =
  let
    convert = HtmlT . Just . runIdentity . runHtmlT
    infoHtml = toList mbUsr iList
    eingHtml = if null eList
               then HtmlT Nothing
               else convert $ foldMap (\e ->
                 eingangDisplay (eingangStrasse e) (eingangNummer e)
                                      ) eList
  in figure_ [ id_ "gebaeude-display" ] $ do
    figcaption_ [] $ do
      h4_ [] "🏢 Gebäude"
    ul_ [] $ do
      li_ [] $ do
        figure_ [class_ "card"] $ do
          ul_ [] $ do
            itemDisplay "str" (Just "Kategorie") False $
              SchG.gebaeudeKategor g
            itemDisplay "str" (Just "Anzahl Wohnungen") False $
              T.pack . show <$> SchG.gebaeudeAnzahlwohnung g
            itemDisplay "str" (Just "Baujahr") False $
              T.pack . show <$> SchG.gebaeudeBaujahr g
            itemDisplay "str" (Just "Fläche m²") True $
              T.pack . show <$> SchG.gebaeudeFlaeche g
            itemDisplay "str" (Just "Volumen m³") True $
              T.pack . show <$> SchG.gebaeudeVolumen g
            itemDisplay "str" (Just "EGID") False $
              Just . toFixed 0 $ SchG.gebaeudeEgid g
      li_ [] $ do
        figure_ [] $ do
          figcaption_ [] $ do
            div_ [ class_ "header buttons" ] $ do
              case mbUsr of
                Just _  -> newButton "info"
                Nothing -> "" :: Html ()
              h4_ [] "Info"
          infoHtml
      case runHtmlT eingHtml of
        Just i  -> eingangFrame $ HtmlT $ Identity i
        Nothing -> mempty

eingangFrame :: Html () -> Html ()
eingangFrame eingHtml = do
  li_ [ id_ "eingang-list" ] $ do
    figure_ [ class_ "card"] $ do
      figcaption_ [] $ do
        h4_ [] "🚪 Eingänge"
      ul_ [] $ do
        eingHtml

eingangDisplay :: T.Text -> T.Text -> Html ()
eingangDisplay str nr = html_ $
  itemDisplay "str" Nothing False $ Just $ T.concat [ str, " ", nr ]
