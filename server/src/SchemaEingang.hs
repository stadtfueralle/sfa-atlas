{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module SchemaEingang where

import           Data.Aeson                  (ToJSON, object, toJSON, (.=))
import           Data.Geometry.Geos.Geometry (Point (..), Some (Some))
import qualified Data.Text                   as T
import           Data.Time                   (UTCTime)
import qualified Database.Persist.Sql        as Sql
import qualified Database.Persist.TH         as PTH

import           PostgisHaskellBinding       (Geo)

PTH.share [ PTH.mkPersist PTH.sqlSettings
          , PTH.mkEntityDefList "entityDefs"
          ]
          [PTH.persistLowerCase|
            Eingang sql=eingang
              geom (Geo Point)
              egaid Int Maybe
              gebaeudeEgid Double
              edid Int
              strasse T.Text
              nummer T.Text
              created UTCTime default=CURRENT_TIMESTAMP
              UniqueEgidEgid gebaeudeEgid edid
              deriving Show
          |]

