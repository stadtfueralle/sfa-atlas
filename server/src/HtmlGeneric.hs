{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlGeneric where

import           Data.Maybe (fromMaybe)
import           Data.Text  (Text)
import qualified Data.Text  as T
import           Lucid

glueJsFunc :: Text -> [Text] -> Text
glueJsFunc name params = T.concat [ name, "(", T.intercalate "," params, ")"]

selectSingleDialog :: [(Text,Text)] -> Html ()
selectSingleDialog available =
  div_ [ class_ "single select" ] $ do
      form_ [ ] $ do
        ul_ [ ] $ do
            foldMap li available
  where 
    li :: (Text, Text) -> Html ()
    li (id', txt) = li_ [] $ do 
                      button_ [ type_ "button"
                              , class_ "value"
                              , onclick_ $ "selectMe("<> id' <>",'"<> txt <>"')"
                              ] $ do
                              span_ [] (toHtml txt)

selectDialog :: Text -> [(Text,Text)] -> [(Text,Text)] -> Html ()
selectDialog attrName selected available =
  div_ [ class_ "multiple select" ] $ do
    form_ [ ] $ do
      button_ [ class_ "save", type_ "button"
              , onclick_ "selectUs(this)"
              ] "Auswahl bestätigen"
      ul_ [ ] $ do
          foldMap (li_ [] . multiCheckbox attrName True) selected
          foldMap (li_ [] . multiCheckbox attrName False) available

selectMultiField :: Text -> Bool -> [(Text,Text)] -> Html ()
selectMultiField attrName required valList =
  let placeholder :: Text
      placeholder = if null valList then "show" else "blank"
  in html_ $ do
      button_ [ class_ "value", type_ "button"
              , onclick_ "openManyChooser(this)"
              , name_ attrName
              ] $ do
               ul_ [] $ do
                 foldl (>>) (li_ [ class_ ( "placeholder " <> placeholder) ] $
                               toHtml ("Bitte wählen" :: Text)
                            ) 
                            (foldValList <$> valList)
    where foldValList = li_ [] . multiCheckbox attrName True

radioCheckbox = checkbox "radio"
multiCheckbox = checkbox "checkbox"

checkbox :: Text -> Text -> Bool -> (Text, Text) -> Html ()
checkbox typ attrName checked (id', val) =
  let inp = input_ [ name_ attrName
                   , class_ "icon clickable"
                   , data_ "value" val
                   , value_ id'
                   , type_ typ
                   ]
      inpChk = if checked
               then with inp [ checked_ ]
               else with inp []
  in label_ [] $ do 
       inpChk
       span_ [] (toHtml val)

selectSingleField :: Text -> (Text,Text) -> Html ()
selectSingleField name (valId,valName) =
  let buttonLabel = if valName == "" then "Bitte wählen" else valName
      inp = input_ [ name_ name
                   , class_ "icon clickable"
                   , value_ valId
                   , type_ "hidden"
                   ]
  in html_ $ do
        div_ [ class_ "error-msg" ] ""
        button_ [ class_ "value", type_ "button"
                , onclick_ "openSingleChooser(this)"
                , name_ name
                ] $ do
                 ul_ [] $ do
                   li_ [] $ do
                     inp
                     span_ [] (toHtml buttonLabel)

listDisplay :: [Html ()] -> Html()
listDisplay list = div_ [ class_ "sfadata list"] $ do
                         ul_ [] $ do
                           if null list
                           then mempty
                           else foldMap (li_ []) list

cmdButton :: Text -> Text -> Text -> Html ()
cmdButton name cmd icon = button_ [ class_ (T.concat [ name, " icon" ])
                                  , onclick_ cmd
                                  , type_ "button"
                                  ] $ span_ [] $ toHtml icon

editButton :: Html ()
editButton = cmdButton "edit" "editMe(this)" "✏"

deleteButton :: Text -> Text -> Html ()
deleteButton id' obj = cmdButton "delete" delCmd "🗑"
  where delCmd = "deleteMe(this,"<> id' <>",'"<> obj <>"')"

newButton :: Text -> Html ()
newButton obj = cmdButton "new" newMe "🌟"
  where newMe = T.concat [ "createEmptyForm('", obj, "')" ]

saveButton :: Html ()
saveButton = cmdButton "save" "saveMe(this)" "💾"

withdrawButton :: Html ()
withdrawButton = cmdButton "withdraw" "withdrawMe(this)" "↩"

itemDisplay :: Text
            -> Maybe Text
            -> Bool
            -> Maybe Text -> Html ()
itemDisplay type' mbLabel localize mbVal =
  let (val, class') = case mbVal of
        Just "" -> ("🕸🕸🕸", "value empty")
        Nothing -> ("🕸🕸🕸", "value empty")
        Just v  -> (v, "value")
      localize' = if localize then "true" else ""
      legend = case mbLabel of
        Just l  -> legend_ [] $ toHtml l
        Nothing -> toHtml ("" :: Text)
  in html_ $ do
    li_ [ data_ "type" type' ]$ do
      legend
      div_ [ class_ class', data_ "localize" localize' ] $ toHtml val

inputNumber :: Text
            -> Text
            -> Bool
            -> Maybe Text
            -> Html ()
inputNumber id' name required mbVal =
  let val   = fromMaybe "🕸🕸🕸" mbVal
      req   = if required then "true" else "false"
  in html_ $ do
      input_ [ class_ "value"
              , name_ name
              , type_ "number"
              , data_ "value-type" "number"
              , id_ id'
              , required_ req
              , value_ val
              ]
      div_ [ class_ "error-msg" ] ""

inputDate :: Text
          -> Text
          -> Bool
          -> Maybe Text
          -> Html ()
inputDate id' name required mbVal =
  let val   = fromMaybe "🕸🕸🕸" mbVal
      req   = if required then "true" else "false"
      in html_ $ do
          input_ [ class_ "value"
                  , name_ name
                  , type_ "date"
                  , data_ "value-type" "date"
                  , id_ id'
                  , required_ req
                  , value_ val
                  ]
          div_ [ class_ "error-msg" ] ""

sTxtInput :: Bool
          -> Text
          -> Text
          -> Maybe Text
          -> Html ()
sTxtInput required id' name mbVal =
  let val   = fromMaybe "🕸🕸🕸" mbVal
      req   = if required then "true" else "false"
  in html_ $ do
    input_ [ class_ "value"
            , name_ name
            , data_ "value-type" "text"
            , id_ id'
            , required_ req
            , value_ val
            ]
    div_ [ class_ "error-msg" ] ""

lTxtInput :: Bool
          -> Text
          -> Text
          -> Maybe Text
          -> Html ()
lTxtInput required id' name mbVal =
  let val   = fromMaybe "🕸🕸🕸" mbVal
      req   = if required then "true" else "false"
  in html_ $ do
    textarea_ [ class_ "value"
              , name_ name
              , id_ id'
              , required_ req
              ] $ toHtml val
    div_ [ class_ "error-msg" ] ""

itemSearchBox :: Html ()
itemSearchBox = do input_ [ id_ "item-search-input"
                          , placeholder_ "Suchbegriff eingeben"
                          , name_ "search"
                          ]

importItemSearchJs :: Html ()
importItemSearchJs  = script_ [ type_ "text/javascript"
                              , src_ "/static/js/item-search.js"
                              ] ("" :: T.Text)
