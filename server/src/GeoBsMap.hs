{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators       #-}

module GeoBsMap where

import           Control.Monad.IO.Class (liftIO)
import           Data.Aeson
import           Data.String.Here       (i)
import qualified Data.Text              as T
import qualified Data.Text.Lazy         as TL
import           GHC.Generics
import           Lucid
import           Servant                (Get, Handler, Server, (:>))
import           Servant.Auth.Server    (Auth, AuthResult (Authenticated))

import           CommonLucid            (HTML, RawHtml (..), importLeaflet,
                                         wrapApp)
import           SchemaUser             (User)

type API auths = (Auth auths User :> Get '[HTML] RawHtml)

data Layer = Layer
  { name          :: T.Text
  , display       :: T.Text
  , color         :: T.Text
  , zindex        :: Int
  , wantsCheckbox :: Bool
  } deriving (Generic, ToJSON)


geoFeatures :: [Layer]
geoFeatures =
  [ Layer "eingang"      "Eingang"      "green"   6000 False
  , Layer "gebaeude"     "Gebäude"      "skyblue" 5000 True
  , Layer "baurecht"     "Baurecht"     "pink"    4000 True
  , Layer "liegenschaft" "Liegenschaft" "magenta" 3000 True
  , Layer "quartier"     "Quartier"     "green"   2000 False
  ]

findLayerByName :: T.Text -> [Layer] -> Layer
findLayerByName n = head . filter (\l' -> name l' == n)

renderMap :: T.Text -> Html ()
renderMap manualJs = div_ [id_ "map-container" ] $ do
  div_ [ id_ "map"] ""
  div_ [ id_ "mapbar", class_ "sidebar" ] $ do
    div_ [ class_ "search mapbar-box"] $ do
      div_ [ class_ "mapbar-box head" ] $ do
        input_ [ id_ "search-input"
               , placeholder_ "Suchbegriff eingeben"
               , name_ "search"
               , onkeyup_ "processSearchInput()"
               ]
        button_ [ id_ "search-btn"
                , class_ "mapbar-box collapse btn open icon"
                , onclick_ "searchButtonClick()"
                ] ""
      searchInput
    div_ [ class_ "layer mapbar-box"] $ do
      div_ [ class_ "mapbar-box head" ] $ do
        label_ [ for_ "layer-btn" ] "Layers"
        button_ [ id_ "layer-btn"
                , class_ "mapbar-box collapse btn close icon"
                , onclick_ "mapbarBoxCollapse('layer')"
                ] ""
      div_ [ id_ "layer-content", class_ "mapbar-box content"] $ do
        ul_ [ class_ "mapbar-box collapse ckb"] layerCheckBoxes
    div_ [ class_ "info mapbar-box" ] $ do
      div_ [ class_ "mapbar-box head" ] $ do
        label_ [ for_ "info-btn" ] "Info"
        button_ [ id_ "info-btn"
                , class_ "mapbar-box collapse btn close icon"
                , onclick_ "mapbarBoxCollapse('info')"
                ] ""
      div_ [ id_ "info-content"
           , class_ "mapbar-box content"
           ] "👈 Bitte Object anklicken."
  importLeaflet
  script_ [ type_ "text/javascript", src_ "/static/js/map.js" ] ("" :: T.Text)
  script_ [ type_ "text/javascript", src_ "/static/js/geo.js" ] ("" :: T.Text)
  script_ [ type_ "text/javascript", src_ "/static/js/search.js" ] ("" :: T.Text)
  script_ [ type_ "text/javascript" ] manualJs

searchInput :: Html ()
searchInput = do
  div_ [ id_ "result-list" ] ""
  div_ [ id_ "feature-find", class_ "blank" ] $ do
    figure_ [ class_ "find card display-props" ] $ do
      figcaption_ [ data_ "type" "str" ] $ do
        span_ [ itemprop_ "object", class_ "value" ] ""
      ul_ [] ""
  li_ [ id_ "prop-find", data_ "type" "text", class_ "blank" ] $ do
    a_ [ href_ "#" ] $ do
      span_ [ class_ "label" ] ""
      span_ [ class_ "value" ] ""

layerCheckBoxes :: Html ()
layerCheckBoxes = do
  foldr (<>) (toHtml ("" :: T.Text)) $ checkBox <$>
    filter wantsCheckbox geoFeatures
  where
    checkBox :: Layer -> Html()
    checkBox layer = html_ $ do
      li_ [ class_ "layer visibility ckb" ] $ do
        input_
          [ type_ "checkbox"
          -- , checked_
          , id_ $ T.append (name layer) "_ckb"
          , class_ "layer visibility ckb"
          , onchange_ [i|setVisibility("${name layer}")(this.checked)|]
          ]
        label_ [ class_ "layer visibility ckb"
               , for_ $ T.append (name layer) "_ckb"
               ] $ do
          div_ [ class_ [i|layer visibility ckb slider ${color layer}|] ] $
                                                                    div_ [ ] ""
          span_ [] $ toHtml (display layer)

jsCommand :: IO T.Text
jsCommand = do
  return $ TL.toStrict $ TL.unlines [ "generateDefaultMap ()"
                                    , "fetchPolygons ()"
                                    , "initGeo ()"
                                    ]

fetchMapHandler :: AuthResult User -> Handler RawHtml
fetchMapHandler authRes = case authRes of
  Authenticated usr -> renderMap' (Just usr)
  _                 -> renderMap' Nothing
  where
    renderMap':: Maybe User -> Handler RawHtml
    renderMap' mbUsr = RawHtml . renderBS . wrapApp  mbUsr . renderMap <$>
                       liftIO jsCommand

server :: Server (API auths)
server = fetchMapHandler
