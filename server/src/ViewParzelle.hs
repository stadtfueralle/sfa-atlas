{-# LANGUAGE FlexibleContexts  #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE OverloadedStrings #-}

module ViewParzelle ( byEgrid
                    , katChooserServer
                    , besSelectDialogServer
                    ) where

import           Control.Monad.IO.Class          (liftIO)
import           Control.Monad.Trans.Except      (throwE)
import           Control.Monad                   (void, (>=>))
import           Data.Int                        (Int64)
import           Data.List                       ((\\), partition)
import           Data.Maybe                      (maybeToList, fromMaybe)
import qualified Data.Text                       as T
import           Data.Time                       (getCurrentTime, UTCTime (..))
import           Database.Esqueleto.Experimental ((==.), (^.), entityKey, entityVal, fromSqlKey, toSqlKey, Entity(Entity), select, delete, from, in_, insertMany_, table, where_, val, valList)
import           Lucid                           (renderBS, Html (..))
import           Servant                         (Handler (..), err401, errBody)

import           CommonLucid                     (RawHtml (..))
import           Database                        as Db
import qualified DatabaseParzelle                as DbParz
import           HtmlGeneric                     (selectDialog, selectSingleDialog, listDisplay)
import           HtmlParzelle                    (parzDisplay)
import qualified HtmlParzelleBesitzende          as BesHtml
import           HtmlParzelleEigentum            (display, form)
import           HtmlParzelleKategorieEigentum   (katDisplay, katForm, katListDisplay)
import qualified SchemaParzelle                  as SchP
import           SchemaUser                      (User)
import           ViewGeneric                     (Save (..), SfaData (..), processSave, SqlSchema (..))
import           ViewParzelleModel               (BesitzendeShow (..),
                                                  BesitzendeSave (..),
                                                  EigentumShow (..),
                                                  EigentumSave (..),
                                                  Kategorie (..),
                                                  -- EigentumKategorieSave (..)
                                                  )

instance Save EigentumSave where
  toSqlRow' (EigentumSave _ dat egrid bem bes besId) =
    SqlEigentum $ SchP.Eigentum (UTCTime dat 0) egrid bes (toSqlKey <$> besId) bem
  screate usr e = do
    eId <- (Db.insertUnique . getEigen . toSqlRow') e
    toDisplay @EigentumShow (Just usr) . head <$> getById eId
  supdate usr e =
    let eId = eInId e
    in do
      (Db.updateRecord' eId . getEigen . toSqlRow') e
      toDisplay @EigentumShow (Just usr) . head <$> getById eId

instance Save BesitzendeSave where
  toSqlRow' (BesitzendeSave _ name bemerk kList strNr plz ort land) = 
    SqlBesitzende $ SchP.Besitzende name strNr plz ort land bemerk
  screate usr b = do
    bId <- (Db.insertUnique . getBes . toSqlRow') b
    saveKat bId (bInKatIdList b) 
    toDisplay @BesitzendeShow (Just usr) . head <$> getById bId
  supdate usr b =
    let bId = bInId b
    in do
      (Db.updateRecord' (bInId b) . getBes . toSqlRow') b
      toDisplay @BesitzendeShow (Just usr) . head <$> getById bId

-- instance Save Kategorie where
--   toSqlRow' (Kategorie eId kId) =
--     SqlBesitzendeKategorie $ SchP.Kategorie (toSqlKey eId) (toSqlKey kId)

instance Save Kategorie where
  toSqlRow' ke = SqlKategorieBesitzende $ SchP.Kategorie $ kName ke
  screate usr ke = do
    keigen <- liftIO $ processSave (Db.insertUnique . getKBes . toSqlRow') ke
    pure $ toDisplay  (Just usr) keigen
  supdate usr ke = do
    keigen <- liftIO $ processSave (Db.updateRecord' (kId ke) . getKBes . toSqlRow') ke
    pure $ toDisplay  (Just usr) keigen

instance SfaData EigentumShow where
  name        = "eigentum"
  getById id' = fmap (assembleEigentum <$>) (DbParz.qEigentumById' id')
  getList     = fmap (assembleEigentum <$>) DbParz.qEigentumList
  emptySfa    = do
    now <- getCurrentTime
    pure $ EigentumShow 0 now "" [] "" "" Nothing Nothing Nothing Nothing
  toFormSfa   = form
  toDisplay   = display
  toList mbUsr iL = listDisplay (toDisplay mbUsr <$> iL)

instance SfaData BesitzendeShow where
  name           = "besitzende"
  getById id'    = fmap (assembleBesitzende <$>) (DbParz.qBesitzendeById id')
  getList        = fmap (assembleBesitzende <$>) DbParz.qBesitzendeList
  emptySfa       = pure $ BesitzendeShow 0 "" "" [] "" "" "" Nothing
  toFormSfa      = BesHtml.form
  toDisplay      = BesHtml.display
  toList mbUsr l = listDisplay (toDisplay mbUsr <$> l) 

  -- toSqlRow     = disassemBesitzende

instance SfaData Kategorie where
  name         = "kategorie-eigentum"
  getById id'  = do
    mbRec <- Db.qRecordById id'
    pure $ Kategorie id' . SchP.kategorieName <$> maybeToList mbRec
  getList      = fmap (uncurry assembleKatEigen <$>) Db.qRecordList'
  emptySfa     = pure $ Kategorie 0 ""
  toFormSfa    = katForm . Just
  toDisplay    = katDisplay
  toList       = katListDisplay

saveKat :: Int64 -> [Int64] -> IO ()
saveKat besId katIdL = do
  current <- getOldKatL besId
  createEntries besId (katIdL \\  current)
  deleteEntries besId (current \\ katIdL)
  pure ()
  where getOldKatL :: Int64 -> IO [Int64]
        getOldKatL eId = do 
          entities <- Db.runActionR . select $ do
                    ekat <- from $ table @SchP.BesitzendeKategorie
                    where_ (ekat ^. SchP.BesitzendeKategorieBesitzendeId ==. val (toSqlKey eId))
                    pure ekat
          pure $ peelKatId <$> entities
        peelKatId :: Entity SchP.BesitzendeKategorie -> Int64
        peelKatId = fromSqlKey . SchP.besitzendeKategorieKategorieId . entityVal
        formEntry :: Int64 -> Int64 -> SchP.BesitzendeKategorie
        formEntry iId katId = SchP.BesitzendeKategorie (toSqlKey iId) (toSqlKey katId)
        createEntries :: Int64 -> [Int64] -> IO ()
        createEntries eigenId katIdList = liftIO . Db.runActionR $
          insertMany_ (fmap (formEntry eigenId) katIdList)
        deleteEntries :: Int64 -> [Int64] -> IO ()
        deleteEntries bId katL = liftIO . Db.runActionR . delete $ do
          kat <- from $ table @SchP.BesitzendeKategorie
          where_ $ kat ^. SchP.BesitzendeKategorieBesitzendeId
                   ==. val (toSqlKey bId)
          where_ $ kat ^. SchP.BesitzendeKategorieKategorieId
                   `in_` valList (toSqlKey <$> katL)


assembleKatEigen :: Int64 -> SchP.Kategorie -> Kategorie
assembleKatEigen i b = Kategorie i (SchP.kategorieName b)

assembleBesitzende :: (Entity SchP.Besitzende, [Entity SchP.Kategorie])
                   -> BesitzendeShow
assembleBesitzende (Entity bKey b, katL) =
  BesitzendeShow (fromSqlKey bKey)
                     (SchP.besitzendeName b)
                     (SchP.besitzendeBemerkung b)
                     (peelKat <$> katL)
                     (SchP.besitzendeStrasse_nr b)
                     (SchP.besitzendePlz b)
                     (SchP.besitzendeOrt b)
                     (SchP.besitzendeLand b)
  where peelKat (Entity k v) = (fromSqlKey k, SchP.kategorieName v)

-- convert Sql Eigentum Model to Html Model for displaying
assembleEigentum :: ( Entity SchP.Eigentum
                    , Maybe (Entity SchP.Besitzende)
                    , [Entity SchP.Kategorie]
                    ) -> EigentumShow
assembleEigentum (Entity iKey i, bes, katList) =
  EigentumShow (fromSqlKey iKey)
           (SchP.eigentumDatum i)
           (SchP.eigentumParzelleEgrid i)
           ((\(Entity katKey kat) -> SchP.kategorieName kat) <$> katList)
           (SchP.eigentumBemerkung i)
           (SchP.eigentumBesitz i)
           (fromSqlKey . entityKey <$> bes)
           (SchP.besitzendeName . entityVal      <$> bes)
           (SchP.besitzendeBemerkung . entityVal <$> bes)
           (SchP.besitzendeOrt . entityVal       <$> bes)

handleParzelle :: Maybe User -> SchP.Parzelle -> Handler RawHtml
handleParzelle mbUsr p = do
  eigentumL <- liftIO $ (assembleEigentum <$>) <$> DbParz.qEigentumByEgrid (SchP.parzelleEgrid p)
  pure . RawHtml . renderBS $ parzDisplay mbUsr p eigentumL

besSelectDialogServer :: User -> [Int64] -> Handler RawHtml
besSelectDialogServer usr id' = do
  allBes <- liftIO $ fmap (assembleBesitzende <$>) DbParz.qBesitzendeList
  pure . RawHtml . renderBS $ selectBesDialog id' allBes
    where
    formBes :: BesitzendeShow -> (T.Text, T.Text)
    formBes b = (T.pack . show $ bOutId b, bOutName b)
    checkMember :: [Int64] -> BesitzendeShow -> Bool
    checkMember idL b = bOutId b `elem` idL
    selectBesDialog :: [Int64] -> [BesitzendeShow] -> Html ()
    selectBesDialog selectedIds all =
      let (selected, available) = partition (checkMember selectedIds) all
      in  selectSingleDialog (formBes <$> all)

katChooserServer :: User -> [Int64] -> Handler RawHtml
katChooserServer usr idList = do
  allKat <- liftIO $ fmap (uncurry assembleKatEigen <$>) Db.qRecordList'
  liftIO . pure . RawHtml . renderBS $ selectKatDialog idList allKat
  where
    selectKatDialog :: [Int64] -> [Kategorie] -> Html ()
    selectKatDialog selectedIds all =
      let (selected, available) = partition (checkMember selectedIds) all
      in  selectDialog "efKatList" (formKatEigen <$> selected) 
                                        (formKatEigen <$> available)
    checkMember :: [Int64] -> Kategorie -> Bool
    checkMember idL ke = kId ke `elem` idL
    formKatEigen :: Kategorie -> (T.Text, T.Text)
    formKatEigen ke = (T.pack . show $ kId ke, kName ke)


byEgrid :: Maybe User -> T.Text -> Handler RawHtml
byEgrid mbUsr egrid = do
  mbParz <- liftIO $ DbParz.qParzelleByEgrid egrid
  case mbParz of
    Just (Entity _ val') -> handleParzelle mbUsr val'
    Nothing              -> Handler err
  where
    err = throwE $ err401 { errBody = "Could not find Parzelle with that egrid" }
