{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module ViewParzelleModel ( EigentumShow (..)
                         , EigentumSave (..)
                         , BesitzendeShow (..)
                         , BesitzendeSave (..)
                         , Kategorie (..)
                         ) where


import           Data.Aeson   (FromJSON, ToJSON)
import           Web.FormUrlEncoded (FromForm)
import           Data.Int     (Int64)
import qualified Data.Text    as T
import           Data.Time    (UTCTime, Day)
import           GHC.Generics (Generic)

data EigentumShow = EigentumShow
  { eOutId        :: Int64
  , eOutDatum     :: UTCTime
  , eOutParzEgrid :: T.Text
  , eOutBesKat    :: [T.Text]
  , eOutBemerk    :: T.Text
  , eOutBesitz    :: T.Text
  , eOutBesId     :: Maybe Int64
  , eOutBesName   :: Maybe T.Text
  , eOutBesBemerk :: Maybe T.Text
  , eOutBesOrt    :: Maybe T.Text
  } deriving (Show, Generic, ToJSON, FromJSON)

data EigentumSave = EigentumSave
  { eInId        :: Int64
  , eInDatum     :: Day
  , eInParzEgrid :: T.Text
  , eInBemerk    :: T.Text
  , eInBesitz    :: T.Text
  , eInBesId     :: Maybe Int64
  } deriving (Show, Generic, ToJSON, FromJSON, FromForm)

data BesitzendeShow = BesitzendeShow
  { bOutId        :: Int64
  , bOutName      :: T.Text
  , bOutBemerk    :: T.Text
  , bOutKatList   :: [(Int64, T.Text)]
  , bOutStrasseNr :: T.Text
  , bOutPlz       :: T.Text
  , bOutOrt       :: T.Text
  , bOutLand      :: Maybe T.Text
  } deriving (Show, Generic, ToJSON, FromJSON)

data BesitzendeSave = BesitzendeSave
  { bInId        :: Int64
  , bInName      :: T.Text
  , bInBemerk    :: T.Text
  , bInKatIdList :: [Int64]
  , bInStrasseNr :: T.Text
  , bInPlz       :: T.Text
  , bInOrt       :: T.Text
  , bInLand      :: Maybe T.Text
  } deriving (Show, Generic, ToJSON, FromJSON, FromForm)

data Kategorie = Kategorie
  { kId   :: Int64
  , kName :: T.Text
  } deriving (Show, Generic, ToJSON, FromJSON, FromForm)
