{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}


module HtmlParzelle where

import           Data.Double.Conversion.Text (toFixed)
import qualified Data.Text                   as T
import           Lucid

import           HtmlGeneric                 (itemDisplay, newButton)
import qualified SchemaParzelle              as SchP
import           SchemaUser                  (User)
import           ViewGeneric                 (SfaData, toList)
import           ViewParzelleModel           (EigentumShow)

parzDisplay :: SfaData EigentumShow => Maybe User
            -> SchP.Parzelle
            -> [EigentumShow]
            -> Html ()
parzDisplay mbUsr p eList =
  let
    eigenHtml = toList mbUsr eList
    title     = T.concat [ "📍 Parzelle - ", SchP.parzelleArt p ]
  in figure_ [ id_ "parzelle-display" ] $ do
    figcaption_ [] $ do
      h4_ [] $ toHtml title
    ul_ [] $ do
      li_ [] $ do
        figure_ [ class_ "card" ] $ do
          ul_ [] $ do
            itemDisplay "str" (Just "EGRID") False $
              Just (SchP.parzelleEgrid p)
            itemDisplay "str" (Just "Fläche m²") True $
              Just . toFixed 0 $ SchP.parzelleArea p
      li_ [] $ do
        figure_ [] $ do
          figcaption_ [] $ do
            div_ [ class_ "header buttons" ] $ do
              case mbUsr of
                Just _  -> newButton "eigentum"
                Nothing -> mempty
              h4_ [] "Besitzanspruch"
          eigenHtml
