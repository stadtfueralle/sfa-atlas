{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators       #-}

module OIDCClient ( API
             , handlers
             ) where

--------------------------------------------------------------------------------
-- Imports:
import           Control.Monad.Except
import qualified Crypto.Hash                as Hash
import           Crypto.Random              (MonadRandom (..), getRandomBytes)
import           Data.Functor               ((<&>))

import qualified Data.Aeson                 as Ae
import           Data.ByteArray.Encoding
import qualified Data.ByteString            as BS
import           Data.ByteString.Builder    (toLazyByteString)
import qualified Data.ByteString.Char8      as Char8
import qualified Data.ByteString.Lazy.Char8 as LChar8
import qualified Data.Text                  as T
import qualified Data.Text.Encoding         as TEnc
import           GHC.Generics               (Generic)
import           Lucid                      (a_, div_, href_, renderBS)
import           Network.HTTP.Client        (newManager)
import           Network.HTTP.Client.TLS    (newTlsManager, tlsManagerSettings)
import           Servant.API
import           Servant.Auth.Server        (CookieSettings (..), JWTSettings,
                                             acceptLogin, clearSession)
import           Servant.Server
import           System.Environment         (getEnv)
import qualified Web.Cookie                 as Cookie
import           Web.Cookie                 (SetCookie (..), defaultSetCookie,
                                             parseCookies, renderSetCookie,
                                             setCookieName)

import           CommonLucid                (HTML, RawHtml (..), appMessage,
                                             appWrapper)
import           Database                   (peelValue)
import           SchemaUser                 (User, fetchUserByEmail)
import qualified Web.OIDC.Client            as O


newtype ProfileClaims = ProfileClaims
    { email :: T.Text
    } deriving (Show, Generic, Ae.FromJSON)


--------------------------------------------------------------------------------
type Index = "index"
  :> Get '[HTML] RawHtml

--------------------------------------------------------------------------------
type Login = "login" :> Get '[HTML] RawHtml

type Logout = "logout" :> Get '[HTML] (Headers '[ Header "Set-Cookie" SetCookie
                                                , Header "Set-Cookie" SetCookie
                                                ] RawHtml)

--------------------------------------------------------------------------------
type Success = "return"
  :> QueryParam "code"  O.Code
  :> QueryParam "state" T.Text
  :> Header "cookie" SessionCookie
  :> Get '[HTML] (Headers '[ Header "Set-Cookie" SetCookie
                           , Header "Set-Cookie" SetCookie
                           ] RawHtml)

--------------------------------------------------------------------------------
type Failed = "return"
  :> QueryParam "error" T.Text
  :> QueryParam "state" T.Text
  :> Header "cookie" SessionCookie
  :> Get '[HTML] (Headers '[ Header "Set-Cookie" SetCookie
                           , Header "Set-Cookie" SetCookie
                           ] RawHtml)

--------------------------------------------------------------------------------
-- | Complete API.
type API = Index :<|> Login :<|> Logout :<|> Success :<|> Failed

--------------------------------------------------------------------------------
-- | A type for getting the session cookie out of the request.
newtype SessionCookie = SessionCookie
  { getSessionCookie :: BS.ByteString
  }

instance FromHttpApiData BS.ByteString where
  parseUrlPiece = Right . TEnc.encodeUtf8

instance FromHttpApiData SessionCookie where
  parseUrlPiece = parseHeader . TEnc.encodeUtf8
  parseHeader bs =
    case lookup "session" (parseCookies bs) of
      Nothing  -> Left "session cookie missing"
      Just val -> Right (SessionCookie val)

initOIDC :: IO O.OIDC
initOIDC = do
  host <- Char8.pack <$> getEnv "APIHOST"
  let clientUri = Char8.concat [ host , "/oidc/return" ]
  clientSec <- Char8.pack <$> getEnv "OIDC_CLIENT_SECRET"
  clientId  <- Char8.pack <$> getEnv "OIDC_CLIENT_ID"
  idP  <- T.pack <$> getEnv "OIDC_IDP_ADDRESS"
  mgr  <- newManager tlsManagerSettings
  prov <- O.discover idP mgr
  pure $ O.setCredentials clientId clientSec clientUri (O.newOIDC prov)

genSecrets' :: forall m. MonadRandom m => m (BS.ByteString, BS.ByteString, BS.ByteString)
genSecrets' = do
  bytes <- getRandomBytes 64 :: m BS.ByteString
  let state = Hash.hash (BS.drop 32 bytes) :: Hash.Digest Hash.SHA256
      nonce = Hash.hash (BS.take 32 bytes) :: Hash.Digest Hash.SHA256
      convert = convertToBase Base64URLUnpadded

  pure (convertToBase Base64URLUnpadded bytes, convert state, convert nonce)

--------------------------------------------------------------------------------
-- | Extract the expected state value from the session cookie.
expectedStateParam :: BS.ByteString -> Either T.Text BS.ByteString
expectedStateParam cookie =
  case extractTokenFromSessionCookie cookie (BS.drop 32) of
    Left err -> Left $ T.concat [ T.pack err
                                , ": "
                                , "Could not extract state from cookie."
                                ]
    Right bs -> Right bs

--------------------------------------------------------------------------------
-- | Given the session cookie, return the expected nonce value.
expectedNonce :: BS.ByteString -> Either T.Text O.Nonce
expectedNonce cookie =
  case extractTokenFromSessionCookie cookie (BS.take 32) of
    Left err -> Left $ T.concat [ T.pack err
                                , ": "
                                , "Could not extract nonce from cookie."
                                ]
    Right bs -> Right bs

--------------------------------------------------------------------------------
-- | Higher-order function of extracting bytes from a session cookie.
extractTokenFromSessionCookie
  :: BS.ByteString                 -- ^ The session cookie
  -> (BS.ByteString -> BS.ByteString) -- ^ Function to extract token bytes
  -> Either String BS.ByteString   -- ^ Error or token.
extractTokenFromSessionCookie cookie f =
    convertFromBase Base64URLUnpadded cookie <&> rehash . f
  where
    rehash :: BS.ByteString -> BS.ByteString
    rehash bs = let hash = Hash.hash bs :: Hash.Digest Hash.SHA256
                in convertToBase Base64URLUnpadded hash

genOIDCURL :: O.OIDC -> BS.ByteString -> BS.ByteString -> IO BS.ByteString
genOIDCURL oidcCreds state nonce = do
  loc <- O.getAuthenticationRequestUrl oidcCreds
                                       [O.openId, O.email, O.profile]
                                       (Just state)
                                       [("nonce", Just nonce)]
  pure $ Char8.pack (show loc)

indexHandler :: Server Index
indexHandler = pure . RawHtml . renderBS $ div_ [] "Test"

loginHandler :: Server Login
loginHandler = do
  (raw, state, nonce) <- liftIO genSecrets' -- generate a random string
  oidcEnv <- liftIO initOIDC
  nUrl <- liftIO $ genOIDCURL oidcEnv state nonce
  cooSettings <- liftIO cookieSettings
  liftIO $ print nUrl
  let coo = defaultSetCookie { setCookieName = "session"
                             , setCookieValue = raw
                             }
      cookie = cooSettings coo
  liftIO (print $ "Redirecting to: " ++ show nUrl) >>
    throwError (err302
      { errHeaders =
          [ ("Location", nUrl)
          , ("Set-Cookie", LChar8.toStrict
              (toLazyByteString (renderSetCookie cookie)))
          ]
      })

tokenToUser :: O.Tokens ProfileClaims -> IO (Maybe User)
tokenToUser token = do
  print token
  let profile = O.otherClaims $ O.idToken token
  usr <- fetchUserByEmail $ email profile
  pure $ peelValue <$> usr
----------------------------------------------------------------------------
-- User returned from provider with a successful authentication.
successHandler :: CookieSettings -> JWTSettings -> Server Success
successHandler cSettings jwtSettings (Just code) (Just state) (Just cookie) = do
  let idpState = TEnc.encodeUtf8 state
      coo      = getSessionCookie cookie
  -- liftIO $ print $ Char8.concat ["AFter session cookie: ", (afterRedirectSessionCookie browser)]
  liftIO $ print $ T.concat ["state: ", state]
  case expectedStateParam coo of
    Left err -> failedHandler (Just err) Nothing Nothing
    Right cookieState -> do
      liftIO $ print $ BS.concat ["coostate: ", cookieState]
      if cookieState /= idpState
        then failedHandler (Just "States do not match") Nothing Nothing
        -- else failedHandler (Just "S do not match") Nothing Nothing
        else do
          case expectedNonce coo of
            Left err -> failedHandler (Just err) Nothing Nothing
            Right nonce -> do -- failedHandler (Just "bla") Nothing Nothing
              oidc <- liftIO initOIDC
              mgr <- liftIO newTlsManager
              token <- liftIO $ O.requestTokens oidc (Just nonce) code mgr
              liftIO $ print token
              mbUsr <- liftIO $ tokenToUser token
              mApplyCookies <- case mbUsr of
                Just usr -> liftIO $ acceptLogin cSettings jwtSettings usr
                Nothing  -> pure Nothing
              case mApplyCookies of
                Just applyCookies -> pure . applyCookies . RawHtml . appWrapper mbUsr
                                     $ appMessage (Just "Login erfolgreich")
                                                  (a_ [ href_ "/"] "Weiter zur Karte...")
                Nothing           -> failedHandler (Just "Missing params") Nothing Nothing
successHandler _ _ _ _ _ = failedHandler (Just "Missing params") Nothing Nothing
  -- creds <- liftIO getCredentials
  -- provider <- liftIO getProvider
  -- now <- liftIO getCurrentTime
  -- tokens <- O.requestTokens oidc
  -- r <- liftIO (authenticationSuccess (https mgr) now provider creds browser)
  -- liftIO $ print "suckHandler"
  -- case r of
  --   Left e       -> throwError (err403 { errBody = LChar8.pack (show e) })
  --   Right _token -> do
  --     let mbEmail = do
  --           let fishClaim name = Ae.fromJSON <$> idToken _token ^. (Jose.unregisteredClaims . at name)
  --           (Ae.Success verified) <- fishClaim "email_verified"
  --           (Ae.Success mail) <- fishClaim "email"
  --           if verified then Just mail else Nothing

-- User returned from provider with an authentication failure.
failedHandler :: Server Failed
failedHandler err _ _ = throwError $
  err400 { errBody = maybe "WTF?" (LChar8.fromStrict . TEnc.encodeUtf8) err }

-- | gen a 302 redirect helper
-- redirects :: (StringConv s ByteString) => s -> Handler ()
--   redirects url = throwError err302 { errHeaders = [("Location",toS url)]}

-- handleLogin :: OIDCEnv -> Handler NoContent
-- handleLogin oidcenv = do
--   loc <- liftIO (genOIDCURL oidcenv)
--   redirects loc
--   return NoContent

-- Redirect the user to the provider.
logoutHandler :: CookieSettings -> Server Logout
logoutHandler cS = pure . clearSession cS . RawHtml . appWrapper Nothing
                        $ appMessage (Just "Logout")
                                     (a_ [ href_ "/" ]  "Weiter zur Karte..")


handlers :: CookieSettings -> JWTSettings -> Server API
handlers cookie jwtSettings = indexHandler
                         :<|> loginHandler
                         :<|> logoutHandler cookie
                         :<|> successHandler cookie jwtSettings
                         :<|> failedHandler

cookieSettings :: IO (SetCookie -> SetCookie)
cookieSettings = do
  env <- getEnv "ENVIRONMENT"
  if env == "development"
  then pure $ \sc -> sc
    { Cookie.setCookiePath     = Just "/oidc/return"
    , Cookie.setCookieHttpOnly = True
    , Cookie.setCookieSecure   = False
    , Cookie.setCookieSameSite = Just Cookie.sameSiteLax
    }
  else pure $ \sc -> sc
    { Cookie.setCookiePath     = Just "/oidc/return"
    , Cookie.setCookieHttpOnly = True
    , Cookie.setCookieSecure   = True
    , Cookie.setCookieSameSite = Just Cookie.sameSiteLax
    }
