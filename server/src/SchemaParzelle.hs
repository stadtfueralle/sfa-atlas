{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module SchemaParzelle where

import           Data.Aeson                  (FromJSON, ToJSON, object, toJSON,
                                              (.=))
import           Data.Geometry.Geos.Geometry (MultiPolygon (..), Some (Some))
import qualified Data.Text                   as T
import           Data.Time
import qualified Database.Persist.TH         as PTH
import           GHC.Generics                (Generic)

import qualified Database                  as Db
import           PostgisHaskellBinding       (Geo)

PTH.share [PTH.mkPersist PTH.sqlSettings, PTH.mkMigrate "migrateAll"] [PTH.persistLowerCase|
Parzelle sql=parzelle
  geom (Geo MultiPolygon)
  simpleGeom (Geo MultiPolygon) generated=ST_Simplify(geom,0.00001,true)
  egrid T.Text
  typ T.Text
  art T.Text
  area Double
  grundbuchkreisnummer T.Text
  grundstuecksnummer T.Text
  created UTCTime default=CURRENT_TIMESTAMP
  UniqueEgrid egrid
  deriving Generic Show

Eigentum sql=eigentum
  datum UTCTime default=CURRENT_TIMESTAMP
  parzelleEgrid T.Text
  besitz T.Text
  besitzende BesitzendeId Maybe OnDeleteSetDefault default=null
  bemerkung T.Text
  Foreign Parzelle fk_eigentum_parzelle parzelleEgrid References egrid
  UniqueEigentum datum parzelleEgrid besitz
  deriving Show Generic ToJSON FromJSON

Besitzende sql=besitzende
  name T.Text
  strasse_nr T.Text
  plz T.Text
  ort T.Text
  land T.Text Maybe
  bemerkung T.Text
  UniqueBesitzende name bemerkung
  deriving Generic Show ToJSON FromJSON

Kategorie sql=kategorie_besitzende
  name T.Text
  UniqueKategorie name
  deriving Show Generic ToJSON FromJSON

BesitzendeKategorie sql=besitzende_kategorie
  besitzendeId BesitzendeId OnDeleteSetDefault default=1
  kategorieId KategorieId OnDeleteCascade OnUpdateCascade
  UniqueBesitzendeKategorie besitzendeId kategorieId
|]


instance Eq Besitzende where
  (Besitzende name1 _ _ _ _ _) == (Besitzende name2 _ _ _ _ _) =
                      name1 == name2
 
instance Ord Besitzende where
  (Besitzende name1 _ _ _ _ _) `compare` (Besitzende name2 _ _ _ _ _) =
                      name1 `compare` name2

instance Eq Kategorie where
  (Kategorie name1) == (Kategorie name2) =
                      name1 == name2
 
instance Ord Kategorie where
  (Kategorie name1) `compare` (Kategorie name2) =
                      name1 `compare` name2

instance Db.SqlClass Besitzende
instance Db.SqlClass Kategorie
instance Db.SqlClass Eigentum
instance Db.SqlClass BesitzendeKategorie
