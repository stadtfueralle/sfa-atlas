{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module ViewGebaeudeModel ( InfoShow (..)
                         , InfoSave (..)
                         , Kategorie (..)
                         ) where


import           Data.Aeson   (FromJSON, ToJSON)
import           Data.Int     (Int64)
import qualified Data.Text    as T
import           Data.Time    (UTCTime, Day)
import           GHC.Generics (Generic)
import           Web.FormUrlEncoded (FromForm)

data InfoShow = InfoShow
  { outId       :: Int64
  , outDatum    :: UTCTime
  , outGebEgid  :: Double
  , outText     :: T.Text
  , outAbsender :: Maybe T.Text
  , outKatList  :: [(Int64, T.Text)]
  } deriving (Show, Generic, ToJSON, FromJSON)

data InfoSave = InfoSave
  { inId        :: Int64
  , inDatum     :: Day
  , inGebEgid   :: Double
  , inText      :: T.Text
  , inAbsender  :: Maybe T.Text
  , inKatIdList :: [Int64]
  } deriving (Show, Generic, ToJSON, FromJSON, FromForm)

data Kategorie = Kategorie
  { kId   :: Int64
  , kName :: T.Text
  } deriving (Show, Generic, ToJSON, FromJSON, FromForm)

