{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}


module CommonLucid where

import qualified Data.ByteString.Lazy as Lazy
import           Data.Text            as T
import           Data.Text.Lazy       as TL
import           Lucid
import           Lucid.Base
import qualified Lucid.Svg            as Svg
import           Network.HTTP.Media   ((//), (/:))
import           Servant              (Accept (..), MimeRender (..))

import           SchemaUser           (User (..))

data HTML

newtype RawHtml = RawHtml { unRaw :: Lazy.ByteString }

instance Accept HTML where
  contentType _ = "text" // "html" /: ("charset", "utf-8")

instance MimeRender HTML RawHtml where
  mimeRender _ = unRaw

favicon :: Svg.Svg ()
favicon = do
  Svg.svg_ [ Svg.version_ "1.1"
           , Svg.viewBox_ "0 0 100 100"
           , makeAttribute "xmlns" "http://www.w3.org/2000/svg"
           ] $ Svg.text_ [Svg.y_ ".9em", Svg.font_size_ "90"] "✊"

wrapHead :: Html () -> Html ()
wrapHead content = html_ $ do
  head_ $ do
    title_ "RAS Comrades"
    meta_ [ name_ "viewport"
          , content_ "width=device-width, initial-scale=1"
          ]
    link_ [ rel_ "icon"
          , href_ $ T.append "data:image/svg+xml," $
            (TL.toStrict . Svg.renderText) favicon
          ]
    link_ [rel_ "stylesheet", type_ "text/css", href_ "/static/css/main.css"]
    link_ [rel_ "stylesheet", type_ "text/css", href_ "/static/css/app.css"]
  body_ $ do
    content


appWrapper :: Maybe User -> Html () -> Lazy.ByteString
appWrapper mbUsr html = renderBS $ wrapApp mbUsr html

wrapApp :: Maybe User -> Html () -> Html ()
wrapApp usr content = html_ $ do
  wrapHead $ do
    link_ [rel_ "stylesheet", type_ "text/css", href_ "/static/css/search.css"]
    link_ [rel_ "stylesheet", type_ "text/css", href_ "/static/css/card.css"]
    script_ [ type_ "text/javascript"
            , src_ "/static/js/webapp.js"
            ] ("" :: T.Text)
    script_ [ type_ "text/javascript"
            , src_ "/static/js/error.js"
            ] ("" :: T.Text)
    script_ [ type_ "text/javascript"
            , src_ "/static/js/buttons.js"
            ] ("" :: T.Text)
    div_ [ id_ "overall-dialog"
         , class_ "blank" -- blank by default and on background-click
         , onclick_ "this.classList.add('blank');this._target={}"] $ do
      div_ [ class_ "dialog window" -- do not blank on foreground-click
           , onclick_ "((ev) => ev.stopPropagation())(event)" ] ""
    div_ [ class_ "app-shell" ] $ do
      appbar usr
      link_ [ rel_ "stylesheet"
            , type_ "text/css"
            , href_ "/static/css/mapbar.css"
            ]
      div_ [ class_ "app main" ] $ do
        content

appMessage :: Maybe T.Text -> Html () -> Html ()
appMessage mbTitle content = div_ [class_ "message"] $ do
  figure_ [ class_ "card" ] $ do
    case mbTitle of
     Just t -> figcaption_ [] $ do
                 div_ [ class_ "register"] $ do
                   h5_ [] $ toHtml t
     Nothing -> mempty
    div_ [] content


userMenu :: Html ()
userMenu = do
  ul_ [ class_ "user menu" ] $ do
    li_ [] $ do
      a_ [ class_ "route menu"
         , href_ "/info/list"
         ] "🏠 Info"
    li_ [] $ do
      a_ [ class_ "route menu"
         , href_ "/kategorie-info/list"
         ] "🏠 Kategorie"
    li_ [] $ do
      a_ [ class_ "route menu"
         , href_ "/eigentum/list"
         ] "📍 Eigentum Info"
    li_ [] $ do
      a_ [ class_ "route menu"
         , href_ "/besitzende/list"
         ] "📍 Besitzende"
    li_ [] $ do
      a_ [ class_ "route menu"
         , href_ "/kategorie-eigentum/list"
         ] "📍 Kategorie Besitzende"
    li_ [] $ do
      a_ [ class_ "route menu"
         , href_ "/"
         ] "🗺️ Karte"

appbar :: Maybe User -> Html ()
appbar mbUsr = header_ [ id_ "appbar" ] $ do
                 img_ [class_ "logo main", src_ "/static/img/logo.png"]
                 div_ [] $ do
                   case mbUsr of
                     Just usr -> do
                       userMenu
                       div_ [ class_ "visitor" ] $ do
                         div_ [ class_ "icon" ] "👶"
                         div_ [ class_ "email" ] (toHtml $ userEmail usr)
                         a_ [ href_ "/oidc/logout" ] $ do
                           button_ [ class_ "logout" ] "🔒"
                     Nothing  -> do
                       a_ [ href_ "/oidc/login", class_ "btn login icon" ] $ do
                         span_ [] "🗝"


importLeaflet :: Html ()
importLeaflet = html_ $ do
      link_
        [ rel_ "stylesheet"
        , type_ "text/css"
        , href_ "/static/leaflet/leaflet.css"
        ]
      link_
        [ rel_ "stylesheet"
        , type_ "text/css"
        , href_ "/static/Leaflet.markercluster/MarkerCluster.css"
        ]
      link_
        [ rel_ "stylesheet"
        , type_ "text/css"
        , href_ "/static/Leaflet.markercluster/MarkerCluster.Default.css"
        ]
      script_
        [ type_ "text/javascript"
        , src_ "/static/leaflet/leaflet.js"
        ] ("" :: T.Text)
      -- script_
      --   [ type_ "text/javascript"
      --   , src_ "/static/Leaflet.Deflate/L.Deflate.js"
      --   ] ("" :: T.Text)
      -- script_
      --   [ type_ "text/javascript"
      --   , src_ "/static/Leaflet.markercluster/leaflet.markercluster.js"
      --   ] ("" :: T.Text)
