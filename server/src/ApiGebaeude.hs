{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module ApiGebaeude where

import           Servant        (Server, (:<|>) (..), (:>))

import qualified ApiGeneric     as Generic
import           SchemaGebaeude (Info (..), KategorieInfo)

type API auths = "info"          :> Generic.API Info auths
            :<|> "kategorie-info" :> Generic.API KategorieInfo auths

server :: Server (API auths)
server = Generic.server :<|> Generic.server
