{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveGeneric        #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE UndecidableInstances #-}

module Auth (API, Protected, Unprotected, server) where

import           Control.Monad.Trans (liftIO)
import           Data.Aeson          (FromJSON, ToJSON)
import           GHC.Generics        (Generic)
import           Lucid               (Html, div_, renderBS, toHtml)
import           Servant             (Get, Handler, Header, Headers, JSON,
                                      NoContent (NoContent), ReqBody, Server,
                                      StdMethod (POST), Verb, err401,
                                      throwError, (:<|>) (..), (:>))
import           Servant.Auth.Server (Auth,
                                      AuthResult (Authenticated, BadPassword, Indefinite, NoSuchUser),
                                      CookieSettings, JWTSettings, SetCookie,
                                      acceptLogin, throwAll)

import           CommonLucid         (HTML, RawHtml (..))
import           SchemaUser          (User (User), userEmail, userName)

data Login = Login { username :: String, password :: String }
   deriving (Eq, Show, Read, Generic)

instance ToJSON Login
instance FromJSON Login

type Protected = "name"  :> Get '[HTML] RawHtml
            :<|> "email" :> Get '[HTML] RawHtml


-- | 'Protected' will be protected by 'auths', which we still have to specify.
protected :: AuthResult User -> Server Protected
-- If we get an "Authenticated v", we can trust the information in v, since
-- it was signed by a key we trust.
protected (Authenticated user) =
       (pure . RawHtml . renderBS) (div_ [] $ toHtml $ userName user)
  :<|> (pure . RawHtml . renderBS) (div_ [] $ toHtml $ userEmail user)
-- Otherwise, we return a 401.
protected NoSuchUser          = throwAll err401
protected BadPassword         = throwAll err401
protected Indefinite          = throwAll err401

type Unprotected = "login" :> ReqBody '[JSON] Login
                           :> Verb 'POST 204 '[JSON]
                              (Headers '[ Header "Set-Cookie" SetCookie
                                        , Header "Set-Cookie" SetCookie
                                        ] NoContent)
              :<|> "raw"   :> Get '[HTML]
                              (Headers '[ Header "Set-Cookie" SetCookie
                                        , Header "Set-Cookie" SetCookie
                                        ] RawHtml)

unprotected :: CookieSettings -> JWTSettings -> Server Unprotected
unprotected cs jwts = checkCreds cs jwts :<|> setCookieHandler jwts cs

type API auths = (Auth auths User :> Protected) :<|> Unprotected

server :: CookieSettings -> JWTSettings -> Server (API auths)
server cs jwts = protected :<|> unprotected cs jwts

-- Here is the login handler
checkCreds :: CookieSettings
           -> JWTSettings
           -> Login
           -> Handler (Headers '[ Header "Set-Cookie" SetCookie
                                , Header "Set-Cookie" SetCookie
                                ] NoContent)
checkCreds cookieSettings jwtSettings login = do
-- checkCreds cookieSettings jwtSettings (Login "Ali Baba" "Open Sesame") = do
   -- Usually you would ask a database for the user info. This is just a
   -- regular servant handler, so you can follow your normal database access
   -- patterns (including using 'enter').
   let usr = User "Ali Baba" "ali@email.com"
   mApplyCookies <- liftIO $ acceptLogin cookieSettings jwtSettings usr
   case mApplyCookies of
     Nothing           -> throwError err401
     Just applyCookies -> return $ applyCookies NoContent
checkCreds _ _ _ = throwError err401


cookieHtml :: Html ()
cookieHtml = div_ [] "I set cookie hehe"

setCookieHandler :: JWTSettings
                 -> CookieSettings
                 -> Handler (Headers
                            '[ Header "Set-Cookie" SetCookie
                             , Header "Set-Cookie" SetCookie
                             ] RawHtml)
setCookieHandler jwtSettings cs = do
   let usr = User "Ali Baba" "ali@email.com"
   mApplyCookies <- liftIO $ acceptLogin cs jwtSettings usr
   case mApplyCookies of
     Nothing           -> throwError err401
     Just applyCookies -> pure . applyCookies . RawHtml . renderBS $ cookieHtml
