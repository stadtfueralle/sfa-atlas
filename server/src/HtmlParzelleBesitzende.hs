{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlParzelleBesitzende ( display
                              , form
                              ) where

import           Data.Maybe        (isJust)
import qualified Data.Text         as T
import           Lucid

import           HtmlGeneric       (deleteButton, editButton,
                                    itemDisplay, lTxtInput, listDisplay,
                                    sTxtInput, saveButton, withdrawButton,
                                    selectMultiField)
import           SchemaUser        (User)
import           ViewGeneric       (SfaData, toDisplay)
import           ViewParzelleModel (BesitzendeShow (..))

display :: Maybe User -> BesitzendeShow -> Html ()
display mbUser bes =
  let object           = "besitzende"
      isEditable       = isJust mbUser
      (id', name, bemerk, katList, strnr, plz, ort, _)
                       = serializeBes' bes
  in figure_ [ class_ "card display"
             , data_ "id" id'
             , data_ "object" object 
             ] $ do
    figcaption_ [] $ do
      if isEditable
        then div_ [ class_ "register"] $ do
               div_ [ class_ "edit-buttons" ] $ do
                  editButton
                  deleteButton id' object
        else mempty
    ul_ [] $ do
      itemDisplay "text" Nothing False $ Just name
      itemDisplay "str" (Just "Kategorie") False $
                             Just $ T.intercalate ", " (snd <$> katList)
      itemDisplay "str" (Just "Adresse") False $ Just strnr
      itemDisplay "str" (Just "PLZ") False $ Just plz
      itemDisplay "str" (Just "Ort") False $ Just ort
      itemDisplay "text" (Just "Bemerkung") False $ Just bemerk

form :: BesitzendeShow -> Html ()
form bes =
  let object           = "besitzende"
      (id', name, bemerk, katL, strnr, plz, ort, land)
                       = serializeBes' bes
  in form_ [ data_ "object" object, data_ "id" id' ] $ do
    figure_ [ class_ "card" ] $ do
      figcaption_ [] $ do
        div_ [ class_ "register"] $ do
          div_ [ class_ "edit-buttons" ] $ do
            saveButton
            withdrawButton
      ul_ [] $ do
        input_ [  name_ "bInId"
                , required_ "true"
                , value_ id'
                , type_ "hidden"
                ]
        li_ [ data_ "type" "text" ] $ do
          legend_ [] "Name"
          sTxtInput True id' "bInName" (Just name)
        li_ [ id_ ("kategorie-besitzende-"<> id')
            , data_ "type" "list"
            ] $ do
          fieldset_ [ form_ ("kategorie-besitzende-" <> id'), name_ "bInKatIdList" ] $ do
            legend_ [] "Kategorie"
            selectMultiField "eInKatIdList" True katL
        li_ [ data_ "type" "text" ] $ do
          legend_ [] "Bemerkung"
          lTxtInput True id' "bInBemerk" (Just bemerk)
        li_ [ data_ "type" "str" ] $ do
          legend_ [] "Anschrift"
          sTxtInput True id' "bInStrasseNr" (Just strnr)
        li_ [ data_ "type" "str" ] $ do
          legend_ [] "Ort"
          sTxtInput True id' "bInOrt" (Just ort)
        li_ [ data_ "type" "str" ] $ do
          legend_ [] "Postleitzahl"
          sTxtInput True id' "bInPlz"  (Just plz)
        li_ [ data_ "type" "str" ] $ do
          legend_ [] "Land"
          sTxtInput False id' "bInLand" land

serializeBes' :: BesitzendeShow -> ( T.Text, T.Text, T.Text
                                       , [(T.Text, T.Text)], T.Text
                                       , T.Text, T.Text, Maybe T.Text
                                       )
serializeBes' b = ( T.pack . show $ bOutId b
                  , bOutName b
                  , bOutBemerk b
                  , map (\(a,b) -> (T.pack $ show a, b)) $ bOutKatList b
                  , bOutStrasseNr b
                  , bOutPlz b
                  , bOutOrt b
                  , bOutLand b
                  )
