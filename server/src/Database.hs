{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TypeFamilies      #-}

module Database ( insertUnique', insertUnique, insertUniqueRawRec, qGeoAtPoint', qGeoInGeo, qGeoInRegion, qRecordById, qRecordList, qRecordList'
                , peelValue, peelValues, runActionR, SqlClass, updateRecord, updateRecord') where

import           Control.Monad.Logger
import           Control.Monad.Reader            (runReaderT)
import           Data.ByteString.Char8           (pack)
import           Data.Geometry.Geos.Geometry     (Geometry (MultiPolygonGeometry, PointGeometry),
                                                  MultiPolygon, Point)
import           Data.Geometry.Geos.Serialize    (writeHex)
import           Data.Int                        (Int64)
import           Data.String.Here                (i, iTrim)
import           Data.Text                       (Text)
import qualified Database.Esqueleto.Experimental (PersistEntity(Key), SqlPersistT, fromSqlKey, Entity(Entity))
import           Database.Persist.Postgresql     (ConnectionString, SqlPersistT, ToBackendKey, SqlBackend, Entity (..), Key, withPostgresqlConn)
import qualified Database.Persist.Sql            as Sql
import qualified Database.Persist.TH             as PTH
import           System.Environment

import           PostgisHaskellBinding           (Geo (..))

type PGInfo = ConnectionString

class (PTH.OnlyOneUniqueKey a, Sql.ToBackendKey Sql.SqlBackend a)
  => SqlClass a

-- access to fresh postgres db, several dbs are running here
sfaConnectionString :: IO String
sfaConnectionString = do
  name <- getEnv "DBNAME"
  port <- getEnv "DBPORT"
  user <- getEnv "DBUSER"
  host <- getEnv "DBHOST"
  pass <- getEnv "DBPASS"
  return $ " dbname   =" ++ name
        ++ " host     =" ++ host
        ++ " port     =" ++ port
        ++ " user     =" ++ user
        ++ " password =" ++ pass

runAction :: PGInfo -> SqlPersistT (LoggingT IO) a -> IO a
runAction connectionString action =
  runStdoutLoggingT $ filterLogger logFilter $ withPostgresqlConn connectionString $ \backend ->
    runReaderT action backend

runActionR :: SqlPersistT (LoggingT IO) a -> IO a
runActionR action = do
  cStr <- pack <$> sfaConnectionString
  runAction cStr action

logFilter :: a -> LogLevel -> Bool
logFilter _ LevelError     = True
logFilter _ LevelWarn      = True
logFilter _ LevelInfo      = True
logFilter _ LevelDebug     = False
logFilter _ (LevelOther _) = True

peelValue :: Sql.Entity record -> record
peelValue (Sql.Entity _ val') = val'

peelValues :: IO [Sql.Entity record] -> IO [record]
peelValues = fmap (fmap (\(Sql.Entity _ val') -> val'))

peelEntity :: (ToBackendKey SqlBackend record) 
           => Entity record -> (Int64, record)
peelEntity (Entity k r) = (Sql.fromSqlKey k, r)

updateRecord'
  :: (Sql.PersistEntity record, Sql.ToBackendKey Sql.SqlBackend record)
  => Int64
  -> record
  -> IO Int64
updateRecord' recId r = do
  runActionR (Sql.replace (Sql.toSqlKey recId) r)
  pure recId

updateRecord
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => Int64
  -> record
  -> IO Int64
updateRecord recId r = do
  runActionR (Sql.replace (Sql.toSqlKey recId) r)
  pure recId

qRecordById
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => Int64
  -> IO (Maybe record)
qRecordById recId = runActionR (Sql.get $ Sql.toSqlKey recId)

qRecordList
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => IO [Sql.Entity record]
qRecordList = runActionR $ Sql.selectList [] []

qRecordList'
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => IO [(Int64, record)]
qRecordList' = fmap (peelEntity <$>) (runActionR $ Sql.selectList [] [])


qGeoInRegion :: (Sql.ToBackendKey Sql.SqlBackend record)
  => Text
  -> Text
  -> IO [Sql.Entity record]
qGeoInRegion geoTable region = runActionR $ Sql.rawSql cmd arg where
    arg = [Sql.PersistText region | region /= "Basel"]
    cmd = if region == "Basel"
          then [iTrim| SELECT ?? FROM ${geoTable}; |]
          else [iTrim| SELECT ?? FROM ${geoTable}
                       WHERE ST_Intersects(geom,(
                         SELECT geom
                         FROM wohnviertel
                         WHERE wov_name = ?
                       ));
                     |]

qGeoInGeo :: (Sql.ToBackendKey Sql.SqlBackend record)
  => Text
  -> Geo MultiPolygon
  -> IO [Sql.Entity record]
qGeoInGeo geoTable (GeoMultiPolygon mp) = do
  cString <- pack <$> sfaConnectionString
  runAction cString $
    Sql.rawSql [i|select ?? from ${geoTable} where st_contains(geom, ?);|]
    [Sql.PersistLiteralEscaped . writeHex $ MultiPolygonGeometry mp (Just 4326)]

qGeoAtPoint' :: (Sql.ToBackendKey Sql.SqlBackend record)
  => Text
  -> Geo Point
  -> IO [Sql.Entity record]
qGeoAtPoint' geoTable (GeoPoint p) = do
  cString <- pack <$> sfaConnectionString
  runAction cString
    $ Sql.rawSql [i|select ?? from ${geoTable} where st_contains(geom, ?);|]
    [Sql.PersistLiteralEscaped . writeHex $ PointGeometry p (Just 4326)]

-- | Insert a unique record or not, if it already exists. Either return the key
-- of the new entity, or the key of the conflicting entity.
insertUnique
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => record
  -> IO Int64
insertUnique b = do
  record <- runActionR (Sql.upsert b [])
  return $ Sql.fromSqlKey $ Sql.entityKey record

insertUniqueRawRec
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => record
  -> IO (Sql.Entity record)
insertUniqueRawRec b = do
  runActionR (Sql.upsert b [])

insertUnique'
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => record
  -> IO record
insertUnique' b = do
  peelValue <$> runActionR (Sql.upsert b [])

delete' :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => Key record -> IO ()
delete' = runActionR . Sql.delete
