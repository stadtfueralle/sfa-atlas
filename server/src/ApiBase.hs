{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators       #-}


module ApiBase where

import           Servant       (Server, (:<|>) (..), (:>))

import qualified ApiGebaeude   as Gebaeude
import qualified ApiGeoFeature as Geo
-- import qualified ApiMapJSON    as MapJ
import qualified ApiParzelle   as Parzelle
import qualified ApiUser       as User

type API auths = "user"     :> User.API auths
            :<|> Parzelle.API auths
            :<|> Gebaeude.API auths
            -- :<|> MapJ.API        -- detailed info byEgrid/Egid
            :<|> "fc" :> Geo.API -- featureCollection for leaflet

server :: Server (API auths)
server = User.server
    :<|> Parzelle.server
    :<|> Gebaeude.server
    -- :<|> MapJ.server
    :<|> Geo.server
