{-# LANGUAGE AllowAmbiguousTypes   #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeOperators         #-}

module ViewGeneric where

import           Control.Monad.IO.Class     (liftIO)
import           GHC.Generics (Generic)
import           Control.Monad.Trans.Except (throwE)
import           Data.Int                   (Int64)
import           Data.Text                  (Text)

import           Lucid
import           Network.HTTP.Media         ((//), (/:))

import           Servant                    (Accept (..), Capture, Get, QueryParams,
                                             Handler (..), JSON, MimeRender (..),
                                             Patch, Post, ReqBody, Server, err401, errBody,
                                             (:<|>) (..), (:>), FormUrlEncoded)

import           CommonLucid                (HTML, RawHtml (..), appWrapper)
import           HtmlGeneric                (importItemSearchJs, itemSearchBox,
                                             newButton)
import           Database.Persist           (PersistEntity)
import           Web.FormUrlEncoded         (FromForm(..), ToForm(..), )

import           SchemaUser                 (User)
import qualified SchemaParzelle             as SchP
import qualified SchemaGebaeude             as SchG
import qualified ViewParzelleModel          as VPM
import qualified Database                   as Db

data SqlSchema = SqlEigentum   { getEigen :: SchP.Eigentum } 
               | SqlBesitzende { getBes :: SchP.Besitzende }
               | SqlKategorieBesitzende { getKBes :: SchP.Kategorie }
               | SqlInfo { getInfo :: SchG.Info }
               | SqlKategorieInfo { getIKat :: SchG.KategorieInfo }

type SaveRoute a = "save" :> 
          ( ReqBody '[FormUrlEncoded] a :> Post '[HTML] RawHtml
       :<|> ReqBody '[FormUrlEncoded] a :> Patch '[HTML] RawHtml
          )

type AppRoute a = "list"        :> Get '[HTML] RawHtml
             :<|> "get"         :> Capture "id" Int64
                                :> Get '[HTML] RawHtml
             :<|> "create-form" :> Get '[HTML] RawHtml
             :<|> "update-form" :> Capture "id" Int64
                                :> Get '[HTML] RawHtml

appServer :: forall a . SfaData a => User -> Server (AppRoute a)
appServer usr = appListWrapper @a usr
           :<|> appSingleWrapper @a usr
           :<|> appCreateHtml @a usr
           :<|> appFormWrapper @a usr
 

type ForeignRelRoute = QueryParams "pre-selected" Int64
                        :> Get '[HTML] RawHtml

type SnippetRoute a = "snippet" :>
    ( "list"        :> Get '[HTML] RawHtml
 :<|> "get"         :> Capture "id" Int64
                    :> Get '[HTML] RawHtml
 :<|> "create"      :> Get '[HTML] RawHtml
 :<|> "update"      :> Capture "id" Int64
                    :> Get '[HTML] RawHtml
    )

snippetServer :: forall a . SfaData a => User -> Server (SnippetRoute a)
snippetServer usr = listHandler @a usr
               :<|> snippetSingleWrapper @a usr
               :<|> emptyForm @a
               :<|> snippetFormWrapper @a

class Save a where
  screate         :: User -> a -> IO (Html ())
  supdate         :: User -> a -> IO (Html ())
  toSqlRow'       :: a -> SqlSchema

class SfaData a where
  name           :: Text
  getById        :: Int64 -> IO [a]
  getList        :: IO [a]
  emptySfa       :: IO a
  toFormSfa      :: a -> Html ()
  toDisplay      :: Maybe User -> a -> Html ()
  toList         :: Maybe User -> [a] -> Html ()

snippetSingleWrapper :: forall a . SfaData a => User -> Int64 -> Handler RawHtml
snippetSingleWrapper usr id' = do
  byId <- byId' @a id'
  pure $ RawHtml $ renderBS $ toDisplay (Just usr) byId

snippetFormWrapper :: forall a . SfaData a => Int64 -> Handler RawHtml
snippetFormWrapper id' = do
  byId <- byId' @a id'
  pure $ RawHtml $ renderBS $ toFormSfa byId

appFormWrapper :: forall a . SfaData a => User -> Int64 -> Handler RawHtml
appFormWrapper usr id' = do
  byId <- byId' @a id'
  pure $ RawHtml $ appWrapper (Just usr) $ toFormSfa byId

appSingleWrapper :: forall a . SfaData a => User -> Int64 -> Handler RawHtml
appSingleWrapper usr id' = do
  byId <- byId' @a id'
  pure $ RawHtml $ appWrapper (Just usr) $ toDisplay (Just usr) byId

appListWrapper :: forall a . SfaData a => User -> Handler RawHtml
appListWrapper usr = do
 list <- liftIO $ toList @a(Just usr) <$> getList
 liftIO $ pure $ RawHtml $ appWrapper (Just usr) $
          figure_ [id_ "sfadata-view"] $ do
            figcaption_ [] $ do
              div_ [ class_ "flex-wrapper"] $ do
                itemSearchBox
                newButton (name @a)
            div_ [class_ "snippet-container"] list
            importItemSearchJs

processSave :: SfaData a => (a -> IO Int64) -> a -> IO a
processSave saveFunc obj = do
  objId <- saveFunc obj
  head <$> getById objId -- TODO head is unsafe, replace with error handling

listHandler :: forall a . SfaData a => User ->  Handler RawHtml
listHandler usr = do
  liftIO (RawHtml . renderBS . toList @a(Just usr) <$> getList)

byIdUsr :: SfaData a => User -> Int64 -> Handler (User, a)
byIdUsr u id' = do
  byId <- byId' id'
  pure (u, byId)

byId' :: SfaData a => Int64 -> Handler a
byId' id' = do
  info <- liftIO $ getById id'
  case info of
    i:_ -> liftIO $ pure i
    _   -> Handler err
  where
    err = throwE $ err401 { errBody = "Could not find record with that ID" }

emptyForm :: forall a . SfaData a => Handler RawHtml
emptyForm = do
    emptyInstance <- liftIO $ emptySfa @a
    pure $ RawHtml $ renderBS $ toFormSfa emptyInstance

appCreateHtml :: forall a . SfaData a => User -> Handler RawHtml
appCreateHtml usr = do
    emptyInstance <- liftIO $ emptySfa @a
    pure $ RawHtml $ appWrapper (Just usr) $ toFormSfa emptyInstance

createHandler :: Save a => User -> a -> Handler RawHtml
createHandler usr obj = liftIO $ RawHtml . renderBS <$> screate usr obj

updateHandler :: Save a => User -> a -> Handler RawHtml
updateHandler usr obj = liftIO $ RawHtml . renderBS <$> supdate usr obj

saveServer :: forall a . Save a => User -> Server (SaveRoute a)
saveServer usr = createHandler usr
            :<|> updateHandler usr

