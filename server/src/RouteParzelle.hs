{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeApplications      #-}

module RouteParzelle where

import           Data.Text           (Text)
import           Servant             ( Capture, Get, Server, (:<|>) (..), (:>)
                                     , ReqBody)
import           Servant.Auth.Server (Auth, AuthResult (Authenticated),
                                      throwAll)

import           CommonLucid         (HTML, RawHtml (..))
import           HttpErrors          (unauthorized)
import           SchemaUser          (User)
import           ViewGeneric         (AppRoute, ForeignRelRoute,
                                      SaveRoute, SnippetRoute,
                                      appServer, saveServer, snippetServer)
import           ViewParzelle        (byEgrid, katChooserServer, besSelectDialogServer)
import           ViewParzelleModel   ( BesitzendeShow, BesitzendeSave
                                     , EigentumShow, EigentumSave, Kategorie)

type EigentumRoute = AppRoute EigentumShow
                :<|> SnippetRoute EigentumShow
                :<|> SaveRoute EigentumSave
                :<|> "eInBesId"  :> ForeignRelRoute


eigentumServer :: User -> Server EigentumRoute
eigentumServer usr = appServer @EigentumShow usr
                :<|> snippetServer @EigentumShow usr
                :<|> saveServer usr
                :<|> besSelectDialogServer usr

type BesitzendeRoute = AppRoute BesitzendeShow
                  :<|> SaveRoute BesitzendeSave
                  :<|> "eInKatIdList" :> ForeignRelRoute
                  :<|> SnippetRoute BesitzendeShow

besitzendeServer :: User -> Server BesitzendeRoute
besitzendeServer usr = appServer @BesitzendeShow usr
                  :<|> saveServer @BesitzendeSave usr
                  :<|> katChooserServer usr
                  :<|> snippetServer @BesitzendeShow usr

type KatEigentumRoute = AppRoute Kategorie
                   :<|> SaveRoute Kategorie
                   :<|> SnippetRoute Kategorie

katEigentumServer :: User -> Server KatEigentumRoute
katEigentumServer usr = appServer @Kategorie usr
                   :<|> saveServer @Kategorie usr
                   :<|> snippetServer @Kategorie usr


type Route = "eigentum"           :> EigentumRoute
        :<|> "kategorie-eigentum" :> KatEigentumRoute
        :<|> "besitzende"         :> BesitzendeRoute
        :<|> "parzelleByEgrid"    :> Capture "egrid" Text
                                  :> Get '[HTML] RawHtml

type API auths = Auth auths User :> Route

server :: AuthResult User -> Server Route
server (Authenticated usr) = eigentumServer usr
                        :<|> katEigentumServer usr
                        :<|> besitzendeServer usr
                        :<|> byEgrid (Just usr)
server _                   = throwAll unauthorized 
                        :<|> throwAll unauthorized
                        :<|> throwAll unauthorized
                        :<|> byEgrid Nothing
