const itemList = document.querySelector('.sfadata.list')
const allItems = itemList.querySelectorAll(':scope > ul > li')
const searchInput = document.getElementById('item-search-input')
const searchBtn = document.getElementById('search-btn')

const searchPatternInItem = item => pattern => {
    const values = item.querySelectorAll('.value')
    // transfrom NodeList to Array and extract text
    const vTxt   = Array.from(values).map(v => v.innerText)
    return vTxt.reduce((acc, val) => {
        return (acc || val.toLowerCase().includes(pattern.toLowerCase()))
    }, false)
}

searchInput.addEventListener('input', event => {
  if (event.target.value.length < 3) return
  allItems.forEach((i, idx) => {
      if (searchPatternInItem (i) (event.target.value)) {
          i.style.display = 'unset';
      } else {
          i.style.display = 'none';
      }
  })
})
