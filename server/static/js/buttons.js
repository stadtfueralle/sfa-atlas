const getHtmlOptions = {
  method: "GET",
  credentials: "same-origin",
  headers: {
    "Content-Type": "text/html; charset=UTF-8",
    "Accept": "text/html"
  },
};

const httpRequest = async (url, opts) => {
	  const response = await fetch(url, opts);
	  if (!response.ok) {
		    const errorMessage = await response.text();
		    throw new Error(errorMessage);
	  }
	  return response;
}

const selectMe = (id, name) => {
  const overall = document.querySelector('#overall-dialog')
  overall._target.querySelector('input').value = id
  overall._target.querySelector('span').textContent = name
  overall.classList.add('blank')
}

const selectUs = (el) => {
  const overall = document.querySelector('#overall-dialog')
  const checked = el.form.querySelectorAll('input[type=checkbox]:checked')
  const checkList = overall._target.querySelector('ul')
  checkList.innerHTML = ""
  Array.from(checked).map (chk => checkList.appendChild (chk.closest ('li')))
  overall.classList.add('blank')
}

const fetchSelectDialogHtml = async (preSelected, name) => {
    const path = name
    const form = new FormData()
    Array.from(preSelected).map(cb => form.append("pre-selected", cb.value))
    const selected = new URLSearchParams(form)
    const fetchOptions = {
        method: "GET",
        credentials: "same-origin",
	  }
    return httpRequest(`${path}?${selected}`, getHtmlOptions)
}

const openManyChooser = async (el) => {
  const overall = document.querySelector('#overall-dialog')
  overall._target = el
  overall.classList.remove('blank')
  const checked = el.querySelectorAll('input[type=checkbox]:checked')
  const path = `/${el.form.dataset.object}/${el.name}`
  try {
    const resp = await fetchSelectDialogHtml(checked, path)
    overall.querySelector('.window').innerHTML = await resp.text()
  } catch (e) {
    console.log(e)
  }
}

const openSingleChooser = async (el) => {
  const overall = document.querySelector('#overall-dialog')
  overall._target = el
  overall.classList.remove('blank')
  const checked = el.querySelectorAll('input')
  const path = `/${el.form.dataset.object}/${el.name}`
  try {
      const resp = await fetchSelectDialogHtml(checked, path)
      overall.querySelector('.window').innerHTML = await resp.text()
  } catch (e) {
      console.log(e)
  }
}

const saveMe = async (el) => {
    // abort if form is not valid
    const form = el.form
    if (!form.checkValidity()) {
        form.querySelectorAll('.invalid').forEach(e => {
            e.classList.remove('invalid')
            e.parentNode.querySelector('.error-msg').innerHTML = ""
        })
        form.querySelectorAll(':invalid').forEach(e => {
            e.classList.add('invalid')
            e.parentNode.querySelector('.error-msg').innerHTML = e.validationMessage
        })
        return
    }

    const payload = new FormData(form)
    const method  = form.dataset.id === "0" ? "POST" : "PATCH"
    try {
        const resp = await httpRequest (`/${form.dataset.object}/save`,
          { method: method
          , credentials: "same-origin"
          , body: new URLSearchParams(payload)
          })
        const display = await resp.text()
        form.parentNode.innerHTML = display
    } catch (e) {
        console.log(`Error: ${e}`)
    }
}

const createEmptyForm = async (object) => {
    if (document.querySelector(`.${object}-0`)) {
        console.log('There is an empty form already.')
        return
    }
    try {
        const resp = await httpRequest (`/${object}/snippet/create`, getHtmlOptions)
        const editHtml = await resp.text()
        const liDiv = document.createElement('li');
        liDiv.innerHTML = editHtml
        const elem = document.querySelector('.sfadata.list ul')
        elem.prepend(liDiv)
        liDiv.focus()
        // document.getElementById(htmlElemId).parentNode.innerHTML = editHtml
    } catch (e) {
        console.log(`Error: ${e}`)
    } finally {
        return
    }
}

const editMe = async (el) => {
  const figure = el.closest('figure')
  const path = `/${figure.dataset.object}/snippet/update/${figure.dataset.id}`
  try {
    const resp = await httpRequest (path, getHtmlOptions)
    const editHtml = await resp.text()
    const parent = figure.parentNode
    parent.innerHTML = editHtml
    // document.getElementById(htmlElemId).parentNode.innerHTML = editHtml
  } catch (e) {
    console.log(`Error: ${e}`)
  }
}

const withdrawMe = async (el) => {
  const form = el.form
  const path = `/${el.form.dataset.object}/snippet/get/${form.dataset.id}`
  if (form.dataset.id === '0') {
    const editElPar = form.parentNode
    editElPar.parentNode.removeChild(editElPar)
    return
  }
  try {
    const resp = await httpRequest (path, getHtmlOptions)
    const showHtml = await resp.text()
    form.parentNode.innerHTML = showHtml
  } catch (e) {
    console.log(`Error: ${e}`)
  }
}

const deleteMe = async (el, id, object) => {
    if (!window.confirm(`${object} mit id ${id} wirklich löschen?`)) return
    const fetchOptions = {
        method: "DELETE",
        credentials: "same-origin",
    }
    try {
        await httpRequest (`/api/${object}/delete/${id}`, fetchOptions)
        const resp = await httpRequest (`/${object}/snippet/list`, getHtmlOptions)
        el.closest('.snippet-container').innerHTML = await resp.text()
    } catch (e) {
        handleError(e)
    }
}
