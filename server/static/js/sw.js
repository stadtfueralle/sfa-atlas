const urlsToBeCached = [
    '/api/fc/gebaeude',
    '/api/fc/baurecht',
    '/api/fc/eingang',
    '/api/fc/liegenschaft',
]

self.addEventListener('install', event => {
    self.skipWaiting()
    console.log('SW state: installing SW')
})

self.addEventListener("activate", (event) => {
    console.log("activating")
});

self.addEventListener('fetch', event => {
    // location -> Worker location "http://localhost:8000/sw?version=v1"
    const version = new URL(location).searchParams.get('version')
    const pathname = (new URL (event.request.url)).pathname
    console.log(`fetch event on ${pathname}`)
    if (event.request.method === 'GET'
        && urlsToBeCached.includes(pathname)
       ) {
        console.log(`acting on get request ${pathname}`)
        event.respondWith((async () => {
            const cache = await caches.open(version)
            const keys = await caches.keys()
            const cachedResponse = await cache.match(event.request)

            if (cachedResponse) {
                console.log(`Found response for\n ${event.request.url}`)
                return cachedResponse
            }
            const response = await fetch(event.request)
            if (!response || response.status !== 200) return response
            const responseToCache = response.clone()
            try {
                await cache.put(event.request, responseToCache)
                console.log(`saved ${event.request.url}`)
            } catch (err) {
                console.log(`Cache open error:\n${err}`)
            }
            return response
        })())
    } else {
        console.log(`Skipping caching for: ${event.request.url}`)
    }
})

self.addEventListener('message', function (event) {
    console.log('got message')
    if (event.data.action === 'skipWaiting') {
        self.skipWaiting()
    }
})


