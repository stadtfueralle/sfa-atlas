let ftCollection = {}
const infoBox = document.getElementById("info-content")

// loads quickly for testing:
// const params = "?region=Rosental"
const params = ""

const getCssColor = (color) => {
    return getComputedStyle(document.documentElement)
        .getPropertyValue(`--${color}`)
}

const features = {
    eingang: {
        searchProps: { address: 'Adresse' },
        style: {
            color: '#000000',
            weight: 1,
            opacity: 0,
            fillOpacity: 0
        },
        zIndex: 6000
    },
    baurecht: {
        searchProps: { egrid: 'EGRID' },
        style: {
            color: '#00000000',
            weight: 1,
            opacity: 0.5,
            fillColor: getCssColor('pink'),
            fillOpacity: 0.4
        },
        zIndex: 4000
    },
    liegenschaft: {
        searchProps: { egrid: 'EGRID' },
        style: {
            color: '#00000000',
            weight: 1,
            opacity: 0.5,
            fillColor: getCssColor('magenta'),
            fillOpacity: 0.4
        },
        zIndex: 3000
    },
    gebaeude: {
        searchProps: { egid: 'EGID' },
        style: {
            color: '#00000000',
            weight: 1,
            opacity: 0.5,
            fillColor: getCssColor('skyblue'),
            fillOpacity: 0.4
        },
        zIndex: 5000
    }
}

const findEingangOfGebaeude = egid => {
    let layers = Object.values(ftCollection.eingang._layers)
    var hits = layers.filter(
        l => l.feature.properties.gebEgid === egid
    )
    return hits
}

const apiFetchJson = async path => {
    let response = await fetch(`/${path}`);

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }
    return await response.json();
}

// add all Features
const addCheckBoxFeatures = async () => {
    return Promise.all([mapEingang(),
                        mapGebaeude(),
                        mapBaurecht(),
                        mapLiegenschaft()])
}

const fetchPolygons = async () => {
  if (navigator.serviceWorker) {
    console.log('found service worker, waiting for its activation')
    // TODO if sw fails, this will be waiting forever
    await navigator.serviceWorker.ready
    console.log('service worker is active.')
  } else {
    console.log('no service worker found.')
  }
  console.log('loading polygons...')
  return addCheckBoxFeatures()
}

const lolightAll = () => {
    Object.entries(ftCollection).map(([key, ft]) => ft.resetStyle())
    infoBox.innerHTML = ""
}

const hilightPoly = layer => layer.setStyle({fillColor: 'goldenrod'})

const hilightPoint = layer => layer.setStyle ( {opacity: 1} )

const selectParz = layer => {
    lolightAll ()
    hilightPoly (layer)
}

const selectGeba = layer => {
    lolightAll ()
    hilightPoly (layer)
    findEingangOfGebaeude(layer.feature.properties.egid).map(e =>
        hilightPoint (e))
}

const fetchParzInfo = async egrid => {
    const fetchOptions = {
        method: "GET",
        credentials: "same-origin",
        headers: {
            "Content-Type": "text/html; charset=UTF-8",
            "Accept": "text/html"
        },
	  };
    const g = await httpRequest (`parzelleByEgrid/${egrid}`, fetchOptions)
    const html = await g.text()
    infoBox.innerHTML = html
}

const fetchGebInfo = async egid => {
    const fetchOptions = {
        method: "GET",
        credentials: "same-origin",
        headers: {
            "Content-Type": "text/html; charset=UTF-8",
            "Accept": "text/html"
        },
	  };
    const g = await httpRequest (`gebaeudeByEgid/${egid}`, fetchOptions)
    const html = await g.text()
    infoBox.innerHTML = html
}

const clickParzelle = async layer => {
  lolightAll ()
  hilightPoly (layer)
  await fetchParzInfo (layer.feature.properties.egrid)
}

const clickGebaeude = async layer => {
  selectGeba (layer)
  await fetchGebInfo (layer.feature.properties.egid)
}

const setVisibility = fcName => value => {
    if (value) {
        ftCollection[fcName].addTo(map)
    } else {
        ftCollection[fcName].remove()
    }
}

const showLayerIfActive = fcName => {
    checkbox = document.getElementById(`${fcName}_ckb`)
    if (checkbox) {
        setVisibility(fcName)(checkbox.checked)
    }
}

const mapEingang = async () => {
    const name = 'eingang'
    const list = await apiFetchJson (`api/fc/${name}${params}`)
    map.createPane(name).style.zIndex = features[name].zIndex
    const geoJson = L.geoJSON(list, {
        // By default simple markers are drawn for GeoJSON Points.
        pointToLayer: (geoJsonPoint, latlng) => L.circleMarker(latlng),
        pane: name,
        style: feature => features[name].style,
        interactive: false
    })
    ftCollection.eingang = geoJson
    setVisibility(name)(true)
}

const mapGebaeude = async () => {
    const name = 'gebaeude'
    const list = await apiFetchJson (`api/fc/${name}${params}`)
    map.createPane(name).style.zIndex = features[name].zIndex
    const geoJson = L.geoJSON(list, {
        pane: name,
        style: feature => features[name].style,
        interactive: true
    })
    ftCollection.gebaeude = geoJson
    geoJson.on('click', async e => clickGebaeude (e.layer))
    document.getElementById(`${name}_ckb`).checked = true
    setVisibility(name)(true)
}

const mapBaurecht = async () => {
    const name = 'baurecht'
    const list = await apiFetchJson (`api/fc/${name}${params}`)
    map.createPane(name).style.zIndex = features[name].zIndex
    const geoJson = L.geoJSON(list, {
        pane: name,
        style: feature => features[name].style,
        interactive: true,
    })
    ftCollection.baurecht = geoJson
    geoJson.on('click', async e => clickParzelle (e.layer))
    document.getElementById(`${name}_ckb`).checked = true
    setVisibility(name)(true)
}

const mapLiegenschaft = async () => {
    const name = 'liegenschaft'
    const list = await apiFetchJson (`api/fc/${name}${params}`)
    map.createPane(name).style.zIndex = features[name].zIndex
    const geoJson = L.geoJSON(list, {
        pane: name,
        style: feature => features[name].style,
        interactive: true,
    })
    ftCollection.liegenschaft = geoJson
    geoJson.on('click', async e => clickParzelle (e.layer))
    document.getElementById(`${name}_ckb`).checked = true
    setVisibility(name)(true)
}

const initGeo = () => {
    map.on('zoomend', function() {
        if (map.getZoom() < 16) {
            Object.entries(ftCollection).map(
                ([key, ft]) => setVisibility(key)(false)
            )
        } else {
            Object.entries(ftCollection).map(
                ([key, ft]) => showLayerIfActive(key)
            )
        }
    });
}
