const tileserver = {
    swiss: 'https://tile.osm.ch/osm-swiss-style/{z}/{x}/{y}.png',
    stamenToner: 'https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png'
}
const createTileLayer = (url) => {
   return L.tileLayer(
       url, {
           maxZoom: 18,
           attribution: 'Open Street Map'
       }
   )
}

const tileLayers = {
    osmSwissLayer: createTileLayer(tileserver.swiss),
    tonerLayer: createTileLayer(tileserver.stamenToner)
}

let currentTileLayer

const searchButtonClick = () => {
    const searchInput = document.getElementById('search-input')
    const searchBox = document.getElementById('result-list')
    if (searchInput.value.length > 0) {
        searchInput.value = ''
        searchBox.innerHTML = ''
    } else searchInput.focus()
}

const mapbarBoxCollapse = (collapsibleName) => {
    let shell = document.getElementById(`${collapsibleName}-content`)
    if (shell.classList.contains('hide')) {
        shell.classList.remove('hide')
        shell.classList.add('show')
    } else {
        shell.classList.remove('show')
        shell.classList.add('hide')
    }
    let btn = document.getElementById(`${collapsibleName}-btn`)
    if (btn.classList.contains('close')) {
        btn.classList.remove('close')
        btn.classList.add('open')
    } else {
        btn.classList.remove('open')
        btn.classList.add('close')
    }
}

const setMapLayer = (tileLayerName) => {
    if (currentTileLayer) map.removeLayer(currentTileLayer)
    map.addLayer(tileLayers[tileLayerName])
    currentTileLayer = tileLayers[tileLayerName]
}

const optionsHtml = `
<div class="leaflet-control-attribution leaflet-control"
<label for="tile-layer-select">Wähle einen Kartenhintergrund</label>
<select id="tile-layer-select" onchange="setMapLayer(this.value)">
  <option value="tonerLayer">s/w Toner</option>
  <option value="osmSwissLayer">osm CH</option>
</select>
</div>
`

function deflateMarkerOptions (f) {
  if (features[f.options.pane]) {
    return {
      radius: 3,
      weight: 0, // border width
      color: features[f.options.pane].style.fillColor
    }
  }
  else {
    console.log(`no panel: ${f.options}`)
    return {
      radius: 2,
      color: '#000000'
    }
  }
}

// const deflate = L.deflate({
//   minSize: 5,
//   markerType: L.circleMarker,
//   markerOptions: deflateMarkerOptions
// });

var generateDefaultMap = () => {
  var saintlouis = { lat: 47.5859, lng: 7.5580 }
  const rooseli = { lat: 47.56810, lng: 7.60210 }
  map = L.map('map', {
    center: L.latLng(rooseli),
    zoom: 15,
    zoomControl: false
  });
  setMapLayer("osmSwissLayer")
  document.querySelector("#map .leaflet-bottom.leaflet-left").innerHTML = optionsHtml
  document.querySelector("#tile-layer-select").value = "osmSwissLayer"

  // deflate.addTo(map)
  return undefined
}

