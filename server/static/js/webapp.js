const VERSION="v1"

const deleteCache = async (key) => {
    await caches.delete(key);
};

const deleteOldCaches = async () => {
    const cacheKeepList = [VERSION];
    const keyList = await caches.keys();
    console.log(`keylist: ${keyList}`)
    const cachesToDelete = keyList.filter((key) => !cacheKeepList.includes(key));
    await Promise.all(cachesToDelete.map(deleteCache));
};

const registerServiceWorker = async () => {
  if ('serviceWorker' in navigator) {
    console.log('webapp found service functionality worker in browser')
    const version = localStorage.getItem('swCacheVersion')
    console.log(`cached version is ${version}, demanded version is ${VERSION}.`)
    if (version !== VERSION) {
      console.log('cache version mismatch, refreshing cache.')
      deleteOldCaches()
      const cache = await caches.open(VERSION);
      localStorage.setItem('swCacheVersion', VERSION)
    }
    try {
      const reg = await navigator.serviceWorker.register(`/sw?version=${VERSION}`, { scope: '/' })
      if (reg.waiting) console.log('SW waiting')
      else if (reg.installing) console.log('SW installing')
      else if (reg.active) console.log('SW already installed')
    } catch (regError) {
      console.log('SW reg failed: ', regError)
    } finally {
    }
  }
}

registerServiceWorker()
