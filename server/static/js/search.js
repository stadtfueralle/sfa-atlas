const searchBox = document.getElementById('result-list')
const searchInput = document.getElementById('search-input')
const searchBtn = document.getElementById('search-btn')

const calcZoomOptions = () => {
    // --------- <-appbar->-------
    // |-------|           |-----|
    // |     | |           |     |
    // |     | |           |     |
    // ---------           |-----|
    //        ^----mapbar->-------
    const o = window.innerWidth/window.innerHeight
    const mapbarW = document.getElementById('mapbar').offsetWidth
    const mapbarH = document.getElementById('mapbar').offsetHeight
    const appbarW = document.getElementById('appbar').offsetWidth
    const appbarH = document.getElementById('appbar').offsetHeight
    return {
        // o > 1 -> landscape:
        //          padding of mapbar width from the right
        //                     appbar height from the top
        // o < 1 -> portrait:
        //          padding of mapbar height from the bottom
        //                     appbar height from the top
        paddingBottomRight: (o > 1) ? [ mapbarW, 0 ] : [ 0, mapbarH ],
        paddingTopLeft:     (o > 1) ? [ 0, appbarH ] : [ 0, appbarH ],
        animate: true,
        duration: 2
    }
}

const sortedIndex = array => prop => value => {
  var low = 0
  high = array.length
  while (low < high) {
    var mid = (low + high) >>> 1;
    // sort numbers with decimal places in strings correctly
    // eg addresses: "Amerbachstrasse 5" < "Amerbachstrasse 45"
    if (array[mid][1].feature.properties[prop].localeCompare(
      value, undefined, { numeric: true }
    )) low = mid + 1;
    else high = mid;
  }
  return low;
}

let debounceTimer = undefined
const timeout = 500

const getFrame = elementId => {
    const frame = document.getElementById(elementId).cloneNode(true)
    frame.removeAttribute("id")
    frame.classList.remove("blank")
    return frame
}

const processSearchInput = () => {
    document.getElementById(`search-btn`).classList.add('close')
    document.getElementById(`search-btn`).classList.remove('open')
    searchBox.innerHTML = ''
    if (searchInput.value.length < 3) return
    if (debounceTimer) clearTimeout(debounceTimer)
    debounceTimer = setTimeout(() => {
        Object.entries(features).map(([obj, ftParams]) => {
            const propKeyL = Object.keys(ftParams.searchProps)
            const layer = search (searchInput.value) (obj) (propKeyL)
            if (layer.length > 0) {
                const featFind = getFrame ('feature-find')
                featFind.querySelector('[itemprop=object]').textContent = obj
                layer.map(([key, value]) => {
                    const propL = displayFind (obj) (value)
                    propL.map(p => featFind.querySelector('ul').appendChild(p))
                })
                searchBox.appendChild(featFind)
            }
        })
        debounceTimer = undefined
    }, timeout);
}

const resetSearch = () => {
    searchInput.value = ''
    searchBox.innerHTML = ''
    searchBtn.classList.add('open')
    searchBtn.classList.remove('close')
}

const search = token => obj => propKeyL => {
  // insert hits at index at right order instead of sorting afterwards
    // let hits = Object.entries(ftCollection[obj]._layers).reduce(
    //   ((accu, value) => {
    //     propKeyL.map(prop => {
    //       const val = String (value[1].feature.properties[prop])
    //       if (val.includes (token)) {
    //         const index = sortedIndex (accu) (prop) (val)
    //         accu.splice(index,0,value)
    //       }
    //       return 0
    //     })
    //     return accu
    //   }), []
    // )
  let hits = Object.entries(ftCollection[obj]._layers).filter(
      ([key, value]) => {
          const res = propKeyL.reduce((acc, prop) => {
              const val = String (value.feature.properties[prop])
              return (acc || val.includes (token))
          }, false)
          return res
      }
  )
  hits.sort((a,b) => {
      const res = propKeyL.reduce((acc, prop) => {
        if (! acc) {
          const aprop = a[1].feature.properties[prop]
          const bprop = b[1].feature.properties[prop]
          // sort numbers with decimal places in strings correctly
          // eg addresses: "Amerbachstrasse 5" < "Amerbachstrasse 45"
          return aprop.localeCompare(bprop, undefined, { numeric: true })
        }
      }, false)
      return res
  })
  return hits
}

const displayFind = obj => value => {
    const propKeyL = Object.entries(features[obj].searchProps)
    return propKeyL.map(([key, val]) => {
        const propFrame = getFrame ('prop-find')
        propFrame.querySelector('.label').textContent = `${val}: `
        propFrame.querySelector('.value').textContent =
            value.feature.properties[key]
        if (obj === 'eingang') Object.entries(ftCollection.gebaeude._layers)
            .filter(([key, l]) =>
                l.feature.properties.egid === value.feature.properties.gebEgid)
            .map(([key, l]) => {
                // propFrame.onmouseover = () => selectGeba (l)
                propFrame.onclick = () => {
                    clickGebaeude (l)
                    map.fitBounds(l.getBounds(), calcZoomOptions());
                    resetSearch ()
                }
            })
        else if (obj == 'gebaeude') {
            // propFrame.onmouseover = () => selectGeba (value)
            propFrame.onclick = () => {
                clickGebaeude (value)
                map.fitBounds(value.getBounds(), calcZoomOptions ());
                resetSearch ()
            }
        }
        else if (obj === 'liegenschaft' || obj === 'baurecht') {
            // propFrame.onmouseover = () => selectParz (value)
            propFrame.onclick = () => {
                clickParzelle (value)
                map.fitBounds(value.getBounds(), calcZoomOptions ());
                resetSearch ()
            }
        }
        return propFrame
    })
}
