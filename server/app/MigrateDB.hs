module Main where

import           System.Environment (getArgs)

import qualified DbRooseli          as Rooseli

main :: IO ()
main = do
  args <- getArgs
  if null args || head args == "--rooseli"
  then putStrLn "migrating rooseli db" >> Rooseli.migrate
  else putStrLn "migrate eiter rooseli or rawGeo"
