module Main where

import           System.Environment (getArgs)

import qualified Server             as S
-- import qualified CacheServer as C
-- import qualified ServerEsq as E

main :: IO ()
main = do
  args <- getArgs
  if null args
  then putStrLn "running production server" >> S.runServer
  else if head args == "dev"
  then putStrLn "running basic server" >> S.runServer
  else putStrLn "Offer nothing or --dev"
--     then putStrLn "Running Cache Server" >> C.runServer
--       else putStrLn "Running Esqueleto Server" >> E.runServer
