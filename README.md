![SFA Logo](server/static/img/logo.png "Stadt für Alle"){width=100px height=100px}

# SFA Atlas

# Local installation

* [Install Nix](https://nixos.org/manual/nix/stable/installation/installing-binary.html)
* [Enable flakes](https://nixos.wiki/wiki/Flakes#Enable_flakes)
* Clone sfa-atlas repository
    ```
    git clone https://gitlab.com/durgabahadur/sfa-atlas.git
    ```
* Move to the repository directory
    ```
    cd sfa-atlas
    ```
* Build the server
    ```
    nix build
    ```

# Run server locally

* Set the OIDC secrets in a file `./server/secrets.sh`
    ```sh
    export OIDC_CLIENT_ID=oidc_id
    export OIDC_CLIENT_SECRET=dont_tell_anyone
    ```
* Enter the sfa-atlas nix shell
    ```
    nix develop
    ```
* Run the server
    ```
    ./result/bin/run-server
    ```
