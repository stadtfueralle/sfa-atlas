{
  description = "SFA-Atlas flake";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-22.11;
    # myvim.url = "/private/nixModule/neovim";
  };
  # outputs = { self, nixpkgs, myvim }:
  outputs = { self, nixpkgs }:
    let
      project = "sfa-atlas";
      system = "x86_64-linux";
      overlays = [
        (self: super: {
          haskellPackages = super.haskellPackages.override {
            overrides = haskellSelf: haskellSuper: {
              geos = self.haskell.lib.dontCheck (self.haskell.lib.unmarkBroken super.haskellPackages.geos);
             # jose-jwt = self.haskell.lib.unmarkBroken super.haskellPackages.jose-jwt;
              sfa-atlas = (self.haskellPackages.callCabal2nix "sfa-atlas" ./server {});
            };
          };
        })
        (self: super: {
          sfaAtlas = (self.haskellPackages.callCabal2nix "sfa-atlas" ./server {});
        })
        (self: super: {
          sfaAtlasExec = self.haskell.lib.justStaticExecutables super.sfaAtlas;
        })
        # (self: super: {
        #   static = ./static;
        # })
      ];
      pkgs = import nixpkgs {
        inherit system;
        overlays = overlays;
      };
      testWrongUser = ''
        echo "sesame will not open"
        curl -v -X POST -H "Content-Type: application/json" \
            -d '{"username":"Ali Baba","password":"Open Sesamo"}' \
            http://127.0.0.1:8000/auth/login/
      '';
      testUser = ''
        echo "sending alibaba"
        curl -v -X POST -H "Content-Type: application/json" \
            -d '{"username":"Ali Baba","password":"Open Sesame"}' \
            http://127.0.0.1:8000/auth/login/
      '';
      addUser = ''
        NAME=Stadtbüro
        EMAIL=kontakt@stadtfueralle.info
        ! [ -v $1 ] && NAME=$1
        ! [ -v $2 ] && EMAIL=$2
        echo "sending user: $NAME with email: $EMAIL"
        curl -X POST -H "Content-Type: application/json" \
            -d "{\"name\": \"$NAME\", \"email\": \"$EMAIL\"}" \
            http://127.0.0.1:8000/api/user/create/
      '';

      llPaths = with pkgs;
        map (x: (callPackage x { inherit stdenv fetchzip; }).outPath) (import ./leaflet.nix);
      llPathsText = with builtins;
        concatStringsSep " " llPaths;

      staticFiles = pkgs.buildEnv {
        name = "staticFiles";
        paths = llPaths ++ [./server/static];
      };

      hp = pkgs.haskellPackages;
      projectShell = hp.shellFor {
        packages = p: [ pkgs.sfaAtlas ] ;
        buildInputs = with pkgs; [
          # hp.apply-refact hp.hlint hp.stylish-haskell hp.hasktags hp.hoogle
          hp.cabal-install
          gdb
          git
          hp.haskell-debug-adapter
          hp.haskell-language-server
          hp.hoogle
          (writeShellScriptBin "runServerInGhci" ''
            ghci -iserver/src/
          '')
          (writeShellScriptBin "collectStatic" ''
            mkdir -p ./static
            cp -ur ./server/static ./
            cp -ur ${(builtins.concatStringsSep "/* " llPaths) + "/* "} ./static/
          '')
          (writeShellScriptBin "addUser" addUser)
          (writeShellScriptBin "testUser" testUser)
          (writeShellScriptBin "testWrongUser" testWrongUser)
        ];
        withHoogle = true;
        shellHook =
          ''
            export PS1='✊\u@${project} \$ '
            collectStatic
            source ./server/env.sh
          '';
      };
    in rec {
      projectDrv = pkgs;
      defaultPackage.${system} = pkgs.sfaAtlasExec;
      packages.${system} = {
        staticFiles = staticFiles;
        atlas = pkgs.sfaAtlas;
        sfa-atlas = pkgs.haskellPackages.sfa-atlas;
      };
      devShell.${system} = pkgs.mkShell {
        packages = [
          # myvim.packages.x86_64-linux.neovim-haskell
        ];
        inputsFrom = [
          projectShell
          # myvim.devShells.x86_64-linux.neovim-haskell
        ];
      };
    };
}
